<?php
declare(strict_types=1);

use App\Enums\Gift;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('gifts', function (Blueprint $table) {
            $table->id();
            $table->string('label')->unique();
            $table->string('url')->unique();
            $table->string('image');
            $table->integer('amount');
            $table->timestamps();
        });
        foreach (Gift::cases() as $gift) {
            $gift->create();
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('gifts');
    }
};
