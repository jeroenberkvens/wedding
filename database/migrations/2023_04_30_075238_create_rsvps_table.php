<?php
declare(strict_types=1);

use App\Enums\DietType;
use App\Enums\Person;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rsvps', function (Blueprint $table) {
            $table->id();
            $table->enum('person', Person::caseValues());
            $table->boolean('plusOne')->default(false);
            $table->string('name');
            $table->string('allergens')->nullable()->default(null);
            $table->enum('dietType', DietType::caseValues())->default(DietType::Normal->value);
            $table->string('diet')->nullable()->default(null);
            $table->boolean('lunch')->default(false);
            $table->boolean('ceremony')->default(false);
            $table->boolean('speech')->default(false);
            $table->boolean('reception')->default(false);
            $table->boolean('dinerClose')->default(false);
            $table->boolean('dinerBistro')->default(false);
            $table->boolean('party')->default(false);
            $table->boolean('breakfast')->default(false);
            $table->timestamps();
            $table->unique(['person', 'plusOne']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rsvps');
    }
};
