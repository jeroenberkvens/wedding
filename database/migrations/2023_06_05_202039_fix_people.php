<?php
declare(strict_types=1);

use App\Enums\Person;
use App\Models\Rsvp;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $renames             = [
            'Myrthe Nowee' => Person::MyrteNowee->value,
            'Mathilde Teerling' => Person::MathildeBerkvens->value,
        ];
        $peopleWithCurrent   = Person::caseValues();
        foreach ($renames as $old => $new) {
            $peopleWithCurrent[] = $old;
        }
        DB::statement("ALTER TABLE users MODIFY COLUMN name ENUM('" . implode("', '", $peopleWithCurrent) . "') NOT NULL");
        DB::statement("ALTER TABLE rsvps MODIFY COLUMN person ENUM('" . implode("', '", $peopleWithCurrent) . "') NOT NULL");
        foreach ($renames as $old => $new) {
            User::whereName($old)->update(['name' => $new]);
            Rsvp::wherePerson($old)->update(['person' => $new]);
        }
        DB::statement("ALTER TABLE users MODIFY COLUMN name ENUM('" . implode("', '", Person::caseValues()) . "') NOT NULL");
        DB::statement("ALTER TABLE rsvps MODIFY COLUMN person ENUM('" . implode("', '", Person::caseValues()) . "') NOT NULL");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
    }
};
