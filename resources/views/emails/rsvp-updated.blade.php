@php
use App\Models\Rsvp;
use App\Models\User;
/** @var User $updated */
/** @var Rsvp $rsvp */
$rsvp = $updated->person->rsvp();
$timeSlots = $updated->person->getGuestType()->getTimeSlots();
$firstTimeSlot = array_shift($timeSlots);

$rows = [
    ['field' => trans('rsvp.email.field'), 'value' => trans('rsvp.email.value')],
    ['field' => trans('rsvp.name'), 'value' => $rsvp->name],
    ['field' => trans('rsvp.planning'), 'value' => view('mail::planning-entry', ['timeSlot' => $firstTimeSlot, 'checked' => $rsvp->{$firstTimeSlot->value}])],
];
foreach ($timeSlots as $timeSlot) {
    $rows[] = ['field' => ' ', 'value' => view('mail::planning-entry', ['timeSlot' => $timeSlot, 'checked' => $rsvp->{$timeSlot->value}])];
}
$rows[] = ['field' => trans('rsvp.allergens'), 'value' => $rsvp->allergens ?? '---'];
$rows[] = ['field' => trans('rsvp.diet-label'), 'value' => trans('rsvp.diet-types.'.$rsvp->dietType->value)];
$rows[] = ['field' => trans('rsvp.diet'), 'value' => $rsvp->diet ?? '---'];

$maxFieldsLength = max(array_map('strlen', array_column($rows, 'field')));
$maxValuesLength = max(array_map('strlen', array_column($rows, 'value')));
['field' => $fieldsHeader, 'value' => $valuesHeader] = array_shift($rows);
@endphp
<x-mail::message>
# @lang('rsvp.email.header')


@lang('rsvp.email.body', ['actor' => $actor->person->rsvp()->name])


<x-mail::table>
<x-mail::row :field="$fieldsHeader" :maxFieldsLength="$maxFieldsLength" :value="$valuesHeader" :maxValuesLength="$maxValuesLength" />
| {{ str_repeat('-', $maxFieldsLength) }} | {{ str_repeat('-', $maxValuesLength) }} |
@foreach($rows as ['field' => $field, 'value' => $value])
<x-mail::row :field="$field" :maxFieldsLength="$maxFieldsLength" :value="$value" :maxValuesLength="$maxValuesLength" />
@endforeach
</x-mail::table>

<x-mail::button :url="route('rsvp')">
@lang('rsvp.email.button')
</x-mail::button>


@lang('rsvp.email.greetings'),<br>
Jeroen & Jenneke
</x-mail::message>
