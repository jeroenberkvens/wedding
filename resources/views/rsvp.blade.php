<x-app-layout>
    <livewire:rsvp-form :providedPerson="$person"/>
    <livewire:register/>
</x-app-layout>
