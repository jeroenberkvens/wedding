@php use App\Enums\Person;use App\Enums\PersonSort; @endphp
<x-app-layout>
    <div class="w-full text-gray-800 leading-normal shadow-md sm:rounded-lg mt-24">
        <h2>@lang('navigation.overview')</h2>
        <table class="text-center">
            <tr>
                <th><a href="{{ url(route('guest-list', PersonSort::Name->getArgs())) }}">@lang('rsvp.name')</a></th>
                <th><a href="{{ url(route('guest-list', PersonSort::Account->getArgs())) }}">@lang('auth.account')</a></th>
                <th><a href="{{ url(route('guest-list', PersonSort::RSVP->getArgs())) }}"><sup>@lang('navigation.rsvp')</sup>/<sub>@lang('rsvp.coming')</sub></a></th>
                <th><a href="{{ url(route('guest-list', PersonSort::PlusOne->getArgs())) }}">@lang('rsvp.plus-one')</a></th>
            </tr>
            @foreach(Person::sorted(PersonSort::get() ?? PersonSort::Name) as $person)
                <tr>
                    <td>
                        <span title="original">{{ $person->value }}</span>
                        @if(trim($person->getName()) !== trim($person->value))
                            <br/>{{ $person->getName() }}
                        @endif
                    </td>
                    <td>
                        @if($person->hasAccount())
                            <span class="text-green-500">✓</span>
                        @else
                            <span class="text-red-500">✗</span>
                        @endif
                    </td>
                    <td>
                        <sup>
                            @if($person->rsvp()->exists)
                                <span class="text-green-500">✓</span>
                            @else
                                <span class="text-red-500">✗</span>
                            @endif
                        </sup>/<sub>
                            {!! $person->comingState()->getHtml() !!}
                        </sub>
                    </td>
                    <td>
                        @if(!$person->hasPlusOne())
                            <span>-</span>
                        @elseif($person->plusOneRsvp()->exists)
                            <span class="text-green-500">✓</span>
                        @else
                            <span class="text-red-500">✗</span>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</x-app-layout>
