@php use App\Enums\Person;use App\Enums\PersonCountType; @endphp
<x-app-layout>
    <div class="w-full text-gray-800 leading-normal shadow-md sm:rounded-lg mt-24">
        <table class="text-center">
            @foreach(PersonCountType::cases() as $personCountType)
                <tr>
                    @if($personCountType === PersonCountType::NoImpliedReaction)
                        <th><a href="{{ url(route('missing')) }}">{{ $personCountType->name }}</a></th>
                    @elseif($personCountType === PersonCountType::NoReaction)
                        <th><a href="{{ url(route('missing', ['show-implied' => true])) }}">{{ $personCountType->name }}</a></th>
                    @else
                        <th>{{ $personCountType->name }}</th>
                    @endif
                    <td>{{ Person::countByType($personCountType) }}</td>
                </tr>
            @endforeach
        </table>
    </div>
    <div class="w-full text-gray-800 leading-normal shadow-md sm:rounded-lg">
        <h2 class="my-8">@lang('navigation.missing')</h2>
        <table class="text-center">
            <tr>
                <th>@lang('rsvp.name')</th>
                <th>@lang('auth.account')</th>
                <th>@lang('contact.phone-number')</th>
            </tr>
            @foreach(Person::missing(request()->get('all', false), !request()->get('without-controllers', false), request()->get('show-implied', false)) as $person)
                <tr>
                    <td id="{{ $person->getName() }}">{{ $person->getName() }}</td>
                    <td>
                        <sup>
                            @if($person->hasAccount())
                                <span class="text-green-500">✓</span>
                            @else
                                <span class="text-red-500">✗</span>
                            @endif
                        </sup>
                        /
                        <sub>{!! $person->comingState()->getHtml() !!}</sub>
                    </td>
                    <td>
                        @if($person->getPhoneNumber() !== '')
                            <a href="tel:{{ $person->getPhoneNumber() }}">{{ $person->getPhoneNumber() }}</a>
                        @else
                            @php($controller = $person->getControllerWithPhoneNumber())
                            @if($controller !== null)
                                <a href="#{{ $controller->getName() }}">{{ $controller->getName() }}</a>
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</x-app-layout>
