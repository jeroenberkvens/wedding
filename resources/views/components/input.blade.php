@props(['disabled' => false])
<!--suppress HtmlFormInputWithoutLabel -->
<input
    {{ $disabled ? 'disabled' : '' }}
    {!! $attributes->except(['list', 'id'])->merge(['class' => 'border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 rounded-md shadow-sm']) !!}
    @if($attributes->has('list'))list="{{ $attributes->get('id') }}-list"@endif
>
@if($attributes->has('list'))
    <datalist id="{{ $attributes->get('id') }}-list">
        @foreach($attributes->get('list') as $value)
            <option value="{{ $value }}">
        @endforeach
    </datalist>
@endif
