<div class="mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg mx-auto sm:max-w-md">
    <h2>@lang('gift-registry.items.'.$selectedGift?->label)</h2>
    <x-validation-errors class="mb-4"/>
    <form method="POST" wire:submit.prevent="registerPurchase">
        @csrf
        <x-form-input model="amount" type="number" min="1" max="8" container.class="name-field" autocomplete="off" library="gift-registry.fields" autofocus/>
        <div class="flex items-center justify-end mt-4">
            <x-secondary-button type="button" class="ml-4" wire:click.prevent="cancelPurchase">@lang('gift-registry.actions.cancel')</x-secondary-button>
            <x-button class="ml-4">@lang('gift-registry.actions.buy')</x-button>
        </div>
    </form>
</div>
