<li class="mr-2 inline-flex">
    <a href="{{ url(route('rsvp.plusOne', $person)) }}" aria-current="page" class="tab @if($active) active @endif">{{ $person->plusOneRsvp()->name ?: '+1' }}</a>
    @if(!$active && !$person->rsvp()->exists && !$person->plusOneRsvp()->exists)
        <span class="relative flex -ml-2 h-3 w-3">
            <span class="animate-ping absolute inline-flex h-full w-full rounded-full bg-sky-400 opacity-75"></span>
            <span class="relative inline-flex rounded-full h-3 w-3 bg-sky-500"></span>
        </span>
    @endif
</li>
