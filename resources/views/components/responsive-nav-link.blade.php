@props(['active'])

@php
    $classes = ($active ?? false) ? 'responsive-nav-link active' : 'responsive-nav-link';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }} x-data="{ clicked: false }" @click="clicked = true" :class="clicked ? 'active' : ''">
    {{ $slot }}
</a>
