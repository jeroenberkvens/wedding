@php use BeTo\Laravel\Helpers\Attributes; @endphp
@props(['model', 'library', 'id' => $model])
<div {{ Attributes::withPrefix($attributes, 'container.') }}>
    <x-label for="{{ $id }}">@lang($library.'.'.$model)</x-label>
    <x-input
        id="{{ $id }}"
        wire:model="{{ $model }}"
        :value="old($model)"
        {{ $attributes->whereDoesntStartWith('container.')->merge(['class' => $errors->has($model) ? 'block mt-1 w-full error' : 'block mt-1 w-full', 'type' => 'text']) }}
    />
    <x-input-error for="{{ $model }}"/>
</div>
