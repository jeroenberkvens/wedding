<footer class="bg-gray-900">
    <div class="container max-w-6xl mx-auto flex items-center px-2 py-8">
        <div class="w-full mx-auto flex flex-wrap items-center">
            <div class="flex w-full md:w-1/2 justify-center md:justify-start text-white font-extrabold">
                <a class="text-base text-gray-200 no-underline hover:no-underline" href="#">
                    <x-rings class="h-12 w-12 mx-2 stroke-white inline fill-white"/>
                    @lang('home.header')
                </a>
            </div>
            <div class="flex w-full pt-2 content-center justify-between md:w-1/2 md:justify-end">
                <ul class="list-reset flex justify-center flex-1 md:flex-none items-center">
                    @foreach(config('menu') as $route => $label)
                        <li><x-nav-link href="{{ route($route) }}" class="text-white" :active="request()->routeIs($route)">@lang($label)</x-nav-link></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</footer>
