@php use BeTo\Laravel\Helpers\Attributes; @endphp
@props(['model', 'library', 'id' => $model])
<div {{ Attributes::withPrefix($attributes, 'container.')->merge(['class' => 'w-full px-3']) }}>
    <x-label for="{{ $model }}">@lang($library.'.'.$model)</x-label>
    <!--suppress HtmlFormInputWithoutLabel -->
    <textarea
        id="{{ $id }}"
        wire:model="{{ $model }}"
        {{ $attributes->whereDoesntStartWith('container.')->merge(['class' => $errors->has($model) ? 'border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 rounded-md shadow-sm error w-full error' : 'border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 rounded-md shadow-sm error w-full', 'type' => 'text']) }}
        wire:model="{{ $model }}"
    >{{ old($model) }}</textarea>
    <x-input-error for="{{ $model }}"/>
    @if($attributes->has('note'))
        <p class="text-gray-600 text-xs italic">{{ $attributes->get('note') }}</p>
    @endif
    @if($attributes->has('note-html'))
        <p class="text-gray-600 text-xs italic">{!! $attributes->get('note-html') !!}</p>
    @endif
</div>
