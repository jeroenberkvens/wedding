@props(['classss'])
<form {{ $attributes }} :class="$classss">
    @csrf
    <x-validation-errors class="mb-4"/>
    {{ $slot }}
</form>
