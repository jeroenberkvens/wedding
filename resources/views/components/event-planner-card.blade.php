<div class="flex w-full items-center font-sans p-8 md:p-24">
    <img class="w-16 h-16 rounded-full mr-4" src="{{ asset('images/chantal.jpg') }}" alt="Avatar of Author">
    <div class="flex-1">
        <p class="text-base font-bold text-base md:text-xl leading-none">@lang('contact.chantal.name')</p>
        <p class="text-gray-600 text-xs md:text-base">@lang('contact.chantal.description', ['email' => view('components.email-link', ['email' => 'chantal@j-j-gaantrouwen.nl'])->render()])</p>
    </div>
    <div class="justify-end">
        <a href="mailto:chantal@j-j-gaantrouwen.nl" class="rounded-button">Email</a>
    </div>
</div>
