@php
    /** @noinspection PhpUndefinedVariableInspection */
    $email = $email ?? $attributes->get('email');
@endphp
<a href="mailto:{{ $email }}" class="whitespace-nowrap underline">{{ $email }}</a>
