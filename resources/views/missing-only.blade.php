@php use App\Enums\ComingState;use App\Enums\GuestType;use App\Enums\Person; @endphp
<x-app-layout>
    <div class="w-full text-gray-800 leading-normal shadow-md sm:rounded-lg mt-24">
        <h2 class="my-8">@lang('navigation.missing')</h2>
        <table class="text-center">
            <tr>
                <th>@lang('rsvp.name')</th>
                <th>@lang('auth.account')</th>
                <th>@lang('contact.phone-number')</th>
            </tr>
            @php($count = 0)
            @foreach(Person::cases() as $person)
                @continue($person->getGuestType() === GuestType::Organizers || $person->comingState() === ComingState::Coming || $person->comingState() === ComingState::NotComing)
                @php($count++)
                <tr>
                    <td id="{{ $person->getName() }}">{{ $person->getName() }}</td>
                    <td>
                        <sup>
                            @if($person->hasAccount())
                                <span class="text-green-500">✓</span>
                            @else
                                <span class="text-red-500">✗</span>
                            @endif
                        </sup>
                        /
                        <sub>{!! $person->comingState()->getHtml() !!}</sub>
                    </td>
                    <td>
                        @if($person->getPhoneNumber() !== '')
                            <a href="tel:{{ $person->getPhoneNumber() }}">{{ $person->getPhoneNumber() }}</a>
                        @else
                            @php($controller = $person->getControllerWithPhoneNumber())
                            @if($controller !== null)
                                <a href="#{{ $controller->getName() }}">{{ $controller->getName() }}</a>
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    <div class="w-full text-gray-800 leading-normal shadow-md sm:rounded-lg">
        {{ $count }}
    </div>
</x-app-layout>
