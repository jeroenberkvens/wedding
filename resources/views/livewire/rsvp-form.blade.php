@php
    use App\Enums\GuestType;
    use App\Enums\Person;
    use App\Models\Rsvp;
    /** @var Person $currentPerson */
    /** @var Person $person */
    /** @var Rsvp $rsvp */
@endphp
@auth()
    <div>
        @if((!empty($currentPerson->controls()) || $person->hasPlusOne()))
            <nav class="mt-20 relative min-h-10" x-data="{ open: false }">
                <div class="flex items-center md:hidden absolute right-0">
                    <button @click="open = ! open" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                        <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                            <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"/>
                            <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"/>
                        </svg>
                    </button>
                    @if(!$currentPerson->otherRsvpsFilled())
                        <span class="relative flex -ml-3 -mt-6 h-3 w-3">
                            <span class="animate-ping absolute inline-flex h-full w-full rounded-full bg-sky-400 opacity-75"></span>
                            <span class="relative inline-flex rounded-full h-3 w-3 bg-sky-500"></span>
                        </span>
                    @endif
                </div>
                <ul :class="{'block': open, 'hidden md:flex': ! open}" class="grow flex flex-col md:flex-row md:flex-wrap justify-center w-full mb-6 text-sm font-medium text-center text-gray-500 border-b border-gray-200 hidden">
                    <li class="mr-2 inline-flex">
                        <a href="{{ url(route('rsvp')) }}" aria-current="page" class="tab @if($routeIsRsvp) active @endif">{{ $currentPerson->rsvp()->name }}</a>
                        @if(!$routeIsRsvp && !$currentPerson->rsvp()->exists)
                            <span class="relative flex -ml-2 h-3 w-3">
                                <span class="animate-ping absolute inline-flex h-full w-full rounded-full bg-sky-400 opacity-75"></span>
                                <span class="relative inline-flex rounded-full h-3 w-3 bg-sky-500"></span>
                            </span>
                        @endif
                    </li>
                    @if($currentPerson->hasPlusOne())
                        <x-plus-one-tab :person="$currentPerson" :active="$person === $currentPerson && $routeIsPlusOne"/>
                    @endif
                    @foreach($currentPerson->controls() as $controlledPerson)
                        @continue($currentPerson->getGuestType() === GuestType::Organizers && !$controlledPerson->rsvp()->exists)
                        <x-controls-tab :person="$controlledPerson" :active="$controlledPerson === $person && $routeIsControls"/>
                        @if($controlledPerson->hasPlusOne())
                            <x-plus-one-tab :person="$controlledPerson" :active="$controlledPerson === $person && $routeIsPlusOne"/>
                        @endif
                    @endforeach
                </ul>
            </nav>
        @else
            <div class="h-24"></div>
        @endif
        <div class="w-full text-gray-800 leading-normal rounded-t">
            @if(!$rsvp->exists)
                <div class="flex justify-center">
                    <div class="max-w-lg w-full bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4" role="alert">
                        <p class="font-bold">@lang('rsvp.save-warning.title')</p>
                        <p>@lang('rsvp.save-warning.explanation')</p>
                    </div>
                </div>
            @endif
            <div class="flex justify-center my-6">
                <x-form class="w-full max-w-lg" wire:submit.prevent="submit">
                    <div class="flex flex-wrap mb-6">
                        <x-form-input model="name" library="rsvp" x-init="$el.focus()" container.class="w-full px-3 mb-6 md:mb-0"/>
                    </div>
                    <div class="flex flex-wrap md:mb-6">
                        <x-label class="px-3">@lang('rsvp.planning')</x-label>
                        <div class="px-3 w-full flex flex-wrap mb-6 md:mb-0">
                            @foreach($person->getGuestType()->getTimeSlots() as $timeSlot)
                                <div class="w-full">
                                    <input wire:model="{{ $timeSlot->value }}" id="time-{{ $timeSlot->value }}" class="mx-2 leading-tight" name="{{ $timeSlot->value }}" type="checkbox" value="{{ $timeSlot->value }}">
                                    <label for="time-{{ $timeSlot->value }}" class="text-sm">@lang('planning.labels.'.$timeSlot->value) ({{ $timeSlot->getTime() }})</label>
                                    <x-input-error for="{{ $timeSlot->value }}" class="pt-2"/>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    @if ($person->getGuestType() !== GuestType::Party)
                        <x-form-textarea model="allergens" library="rsvp" note-html="{{ trans('rsvp.allergens-note') }}" container.class="flex flex-wrap mb-6"/>
                        <div class="flex flex-wrap md:mb-6">
                            <x-label class="px-3">@lang('rsvp.diet-label')</x-label>
                            <div class="px-3 w-full flex flex-wrap mb-6 md:mb-0">
                                @foreach(['normal', 'vegetarian', 'vegan'] as $type)
                                    <div class="w-full md:w-1/3">
                                        <input wire:model="dietType" id="diet-type-{{ $type }}" class="mr-2 leading-tight" name="dietType" type="radio" value="{{ $type }}">
                                        <label for="diet-type-{{ $type }}" class="text-sm">@lang('rsvp.diet-types.'.$type)</label>
                                    </div>
                                @endforeach
                                <x-input-error for="dietType" class="pt-2"/>
                            </div>
                        </div>
                        <div class="flex flex-wrap mb-6">
                            <x-form-textarea model="diet" library="rsvp" note="{{ trans('rsvp.diet-note') }}"/>
                        </div>
                    @endif
                    <x-button class="ml-4" type="submit">@lang('rsvp.save')</x-button>
                    <p class="px-3 mt-4">@lang('rsvp.notice')</p>
                </x-form>
            </div>
        </div>
    </div>
@else
    <div></div>
@endif
