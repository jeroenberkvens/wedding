@php use App\Models\Rsvp; @endphp
<nav x-data="{ open: false }" class="bg-white border-b border-gray-100">
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl mx-auto px-4 md:px-6">
        <div class="flex justify-between h-16">
            <div class="flex">
                <!-- Navigation Links -->
                <div class="hidden space-x-8 md:-my-px md:ml-6 md:flex">
                    <x-nav-link href="{{ route('overview') }}" :active="request()->routeIs('overview')">@lang('navigation.overview')</x-nav-link>
                </div>
                <div class="hidden space-x-8 md:-my-px md:ml-6 md:flex">
                    <x-nav-link href="{{ route('gift-registry-overview') }}" :active="request()->routeIs('gift-registry-overview')">@lang('navigation.gift-registry-overview')</x-nav-link>
                </div>
                <div class="hidden space-x-8 md:-my-px md:ml-6 md:flex">
                    <x-nav-link href="{{ route('guest-list') }}" :active="request()->routeIs('guest-list')">@lang('navigation.guest-list')</x-nav-link>
                </div>
                <div class="hidden space-x-8 md:-my-px md:ml-6 md:flex">
                    <x-nav-link href="{{ route('missing') }}" :active="request()->routeIs('missing')">@lang('navigation.missing')</x-nav-link>
                </div>
                @if(Rsvp::first()?->exists)
                    <div class="hidden space-x-8 md:-my-px md:ml-6 md:flex">
                        <x-nav-link href="{{ route('rsvp.control', [Rsvp::first()->person]) }}" :active="request()->is('rsvp/*')">@lang('navigation.rsvp')</x-nav-link>
                    </div>
                @endif
            </div>
            <div class="hidden space-x-8 md:-my-px md:ml-6 md:flex">
                <button class="nav-link" type="button" wire:click="toggleLanguage">@lang('navigation.language-toggle')</button>
                <button class="nav-link" type="button" wire:click="logout">@lang('auth.logout')</button>
            </div>

            <!-- Hamburger -->
            <div class="-mr-2 flex items-center md:hidden">
                <button @click="open = ! open" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"/>
                        <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"/>
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <!-- Responsive Navigation Menu -->
    <div :class="{'max-h-screen': open }" class="md:hidden max-h-0 overflow-hidden transition-all ease-in-out duration-500 md:transition-none" :aria-hidden="!open">
        <!-- Navigation Links -->
        <div class="pt-2 pb-3 space-y-1">
            <x-responsive-nav-link href="{{ route('overview') }}" :active="request()->routeIs('overview')">@lang('navigation.overview')</x-responsive-nav-link>
        </div>
        <div class="pt-2 pb-3 space-y-1">
            <x-responsive-nav-link href="{{ route('gift-registry-overview') }}" :active="request()->routeIs('gift-registry-overview')">@lang('navigation.gift-registry-overview')</x-responsive-nav-link>
        </div>
        <div class="pt-2 pb-3 space-y-1">
            <x-responsive-nav-link href="{{ route('guest-list') }}" :active="request()->routeIs('guest-list')">@lang('navigation.guest-list')</x-responsive-nav-link>
        </div>
        <div class="pt-2 pb-3 space-y-1">
            <x-responsive-nav-link href="{{ route('missing') }}" :active="request()->routeIs('missing')">@lang('navigation.missing')</x-responsive-nav-link>
        </div>
        @if(Rsvp::first()?->exists)
            <div class="pt-2 pb-3 space-y-1">
                <x-responsive-nav-link href="{{ route('rsvp.control', [Rsvp::first()->person]) }}" :active="request()->is('rsvp/*')">@lang('navigation.rsvp')</x-responsive-nav-link>
            </div>
        @endif
        <div class="pt-2 pb-3 space-y-1">
            <button class="responsive-nav-link" type="button" wire:click="toggleLanguage">@lang('navigation.language-toggle')</button>
            <button class="responsive-nav-link" type="button" wire:click="logout">@lang('auth.logout')</button>
        </div>
    </div>
</nav>
