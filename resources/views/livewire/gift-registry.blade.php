@php use App\Models\Gift;use Illuminate\Support\Facades\Auth; @endphp
<div x-data="{ showModal: @entangle('showModal').defer, login: false }">
    <div class="flex h-full bg-white rounded-b overflow-hidden shadow-md mb-10 mt-24">
        <div class="w-full flex flex-col flex-grow flex-shrink">
            <div class="flex-1 bg-white overflow-hidden shadow-md p-6 text-center">
                <h2>@lang('gift-registry.intro.header')</h2>
                @foreach(trans('gift-registry.intro.descriptions') as $paragraph)
                    <p class="pt-2">{{ $paragraph }}</p>
                @endforeach
            </div>
        </div>
    </div>
    <div class="relative overflow-x-auto shadow-md sm:rounded-lg" x-data="{}">
        <table class="text-center">
            <thead>
            <tr>
                <th scope="col" colspan="2">@lang('gift-registry.headers.item')</th>
                <th scope="col"><sup>@lang('gift-registry.headers.bought')</sup>/<sub>@lang('gift-registry.headers.total')</sub></th>
                <th scope="col">@lang('gift-registry.headers.actions')</th>
            </tr>
            </thead>
            <tbody>
            @foreach(Gift::myPurchasedGifts() as $gift)
                <tr>
                    <td class="p-0" style="width: 80px">
                        <img class="w-20 inline" src="{{ url($gift->image) }}" alt="gift preview"/>
                    </td>
                    <td class="py-0">
                        <x-external-link href="{{ $gift->url }}"><span>@lang('gift-registry.items.'.$gift->label)</span></x-external-link>
                    </td>
                    <td @class(['text-green-500' => $gift->getPurchaseCount() === $gift->amount])><sup>{{ $gift->getPurchaseCount() }}</sup>/<sub>{{ $gift->amount }}</sub></td>
                    <td>
                        @if($gift->myPurchase() !== null)
                            <span>@lang('gift-registry.actions.bought', ['count' => $gift->myPurchase()->amount])</span><br/>
                        @endif
                        <button class="rounded-button" wire:click="openModal({{ $gift->id }})">
                            @if(Auth::user() === null)
                                @lang('auth.register')
                            @elseif($gift->myPurchase() !== null)
                                @lang('gift-registry.actions.edit')
                            @else
                                @lang('gift-registry.actions.buy')
                            @endif
                        </button>
                    </td>
                </tr>
            @endforeach
            @foreach(Gift::withoutPurchasedAndFullyPurchased() as $gift)
                <tr>
                    <td class="p-0" style="width: 80px">
                        <img class="w-20 inline" src="{{ url($gift->image) }}" alt="gift preview"/>
                    </td>
                    <td class="py-0">
                        <x-external-link href="{{ $gift->url }}"><span>@lang('gift-registry.items.'.$gift->label)</span></x-external-link>
                    </td>
                    <td><sup>{{ $gift->getPurchaseCount() }}</sup>/<sub>{{ $gift->amount }}</sub></td>
                    <td>
                        @if($gift->myPurchase() !== null)
                            <span>@lang('gift-registry.actions.bought', ['count' => $gift->myPurchase()->amount])</span><br/>
                        @endif
                        <button class="rounded-button" wire:click="openModal({{ $gift->id }})">
                            @if(Auth::user() === null)
                                @lang('auth.register')
                            @elseif($gift->myPurchase() !== null)
                                @lang('gift-registry.actions.edit')
                            @else
                                @lang('gift-registry.actions.buy')
                            @endif
                        </button>
                    </td>
                </tr>
            @endforeach
            @foreach(Gift::fullyPurchasedWithoutPurchased() as $gift)
                <tr>
                    <td class="p-0" style="width: 80px">
                        <img class="w-20 inline" src="{{ url($gift->image) }}" alt="gift preview"/>
                    </td>
                    <td class="py-0">
                        <x-external-link href="{{ $gift->url }}"><span>@lang('gift-registry.items.'.$gift->label)</span></x-external-link>
                    </td>
                    <td class="text-green-500"><sup>{{ $gift->getPurchaseCount() }}</sup>/<sub>{{ $gift->amount }}</sub></td>
                    <td>
                        @if($gift->myPurchase() !== null)
                            <span>@lang('gift-registry.actions.bought', ['count' => $gift->myPurchase()->amount])</span><br/>
                        @endif
                        <button class="rounded-button" wire:click="openModal({{ $gift->id }})">
                            @if(Auth::user() === null)
                                @lang('auth.register')
                            @elseif($gift->myPurchase() !== null)
                                @lang('gift-registry.actions.edit')
                            @else
                                @lang('gift-registry.actions.buy')
                            @endif
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <x-base-modal id="registerModal" show="showModal" width="">
            @if(Auth::user() === null)
                <livewire:register/>
            @else
                <x-buy-form :selectedGift="$selectedGift"/>
            @endif
        </x-base-modal>
    </div>
</div>
