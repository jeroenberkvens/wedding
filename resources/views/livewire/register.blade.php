@auth
    <div></div>
@else
    <div class="mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg mx-auto sm:max-w-md" @authentication-changed.window="open = false" x-data="{ login: @entangle('login').defer }">
        <x-validation-errors class="mb-4"/>
        <form method="POST" wire:submit.prevent="registerOrLogin" :class="{'hidden': login}">
            @csrf
            <x-form-input model="name" library="rsvp" autofocus autocomplete="name" container.class="name-field" :list="$names"/>
            <div class="mt-4">
                <x-label for="email-register">@lang('auth.email')</x-label>
                <x-input id="email-register" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autocomplete="username" wire:model="email"/>
            </div>
            <div class="flex items-center justify-end mt-4">
                <button type="button" class="link-button" @click="login = true; setTimeout(() => document.getElementById('email-login').focus())">@lang('auth.already-registered')</button>
                <x-button class="ml-4">@lang('auth.register')</x-button>
            </div>
        </form>
        <form method="POST" wire:submit.prevent="registerOrLogin" class="hidden" :class="{'hidden': !login}">
            @csrf
            <div class="mt-4">
                <x-label for="email-login">@lang('auth.email')</x-label>
                <x-input id="email-login" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autocomplete="username" wire:model="email"/>
            </div>
            <div class="flex items-center justify-end mt-4">
                <button type="button" class="link-button" @click="login = false; setTimeout(() => document.getElementById('name-register').focus())">@lang('auth.register')</button>
                <x-button class="ml-4">@lang('auth.login')</x-button>
            </div>
        </form>
    </div>
@endauth
