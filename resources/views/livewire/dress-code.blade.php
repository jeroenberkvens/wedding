<div class="w-full text-gray-800 leading-normal rounded-t">
    <div class="flex flex-wrap justify-between pt-12 -mx-6" x-data="dressCode('{{ url(asset('/images/tuxido.jpg')) }}', {{ $showWarning ? 'true' : 'false' }})">
        <div class="w-full mt-6 p-4">
            <h1 class="text-center font-bold text-3xl md:text-5xl">@lang('dress-code.title')</h1>
            <p class="text-gray-800 text-base px-6 mt-4 text-center">@lang('dress-code.description', ['email' => view('components.email-link', ['email' => 'jeroen@j-j-gaantrouwen.nl'])->render()])</p>
        </div>

        <!--1/2 col -->
        <div class="w-full md:w-1/2 p-4 flex flex-col flex-grow flex-shrink">
            <div class="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow-lg">
                <div class="flex flex-wrap no-underline hover:no-underline">
                    <img
                        @click="url = $el.src; modalOpen = true"
                        src="{{ url(asset('/images/black-tie-dress-code.webp')) }}"
                        class="h-full w-full rounded-t pb-6 cursor-pointer"
                        alt="Black Tie infographic"
                    >
                    <div class="w-full font-bold text-xl text-gray-900 px-6">@lang('dress-code.men.title')</div>
                    <p class="text-gray-800 text-base px-6 mb-5">@lang('dress-code.men.description')</p>
                </div>
            </div>
        </div>

        <!--1/2 col -->
        <div class="w-full md:w-1/2 p-4 flex flex-col flex-grow flex-shrink">
            <div class="flex-1 flex-row bg-white rounded-t rounded-b-none overflow-hidden shadow-lg">
                <div class="flex flex-wrap no-underline hover:no-underline">
                    <img
                        @click="url = $el.src; modalOpen = true"
                        src="{{ url(asset('/images/tuxido.jpg')) }}"
                        class="h-full w-full rounded-t pb-6 cursor-pointer"
                        alt="Tuxido"
                    >
                    <div class="w-full font-bold text-xl text-gray-900 px-6">@lang('dress-code.renting.title')</div>
                    <p class="text-gray-800 text-base px-6 mb-5">@lang('dress-code.renting.description', ['blackTie' => view('components.black-tie-link')->render()])</p>
                </div>
            </div>
        </div>

        <!--2/3 col -->
        <div class="w-full md:w-2/3 p-4 flex flex-col flex-grow flex-shrink">
            <div class="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow-lg">
                <div class="flex flex-wrap no-underline hover:no-underline">
                    <img
                        @click="url = $el.src; modalOpen = true"
                        src="{{ url(asset('/images/dresses.png')) }}"
                        class="h-full w-full rounded-t pb-6 cursor-pointer"
                        alt="Black Tie Dresses"
                    >
                    <div class="w-full font-bold text-xl text-gray-900 px-6">@lang('dress-code.women.title')</div>
                    <p class="text-gray-800 text-base px-6 mb-5">@lang('dress-code.women.description')</p>
                </div>
            </div>
        </div>

        <!--1/3 col -->
        <div class="w-full md:w-1/3 p-4 flex flex-col flex-grow flex-shrink">
            <div class="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow-lg">
                <div class="flex flex-wrap no-underline hover:no-underline">
                    <video loop autoplay>
                        <source src="https://cdn-3.jjshouse.com/upimg/s1140/30/29/246472b30778ef3ef97fbfc43ddd3029.mp4" type="video/mp4" />
                    </video>
                    <video loop autoplay>
                        <source src="https://cdn-4.jjshouse.com/upimg/s1140/f1/c9/28635c293c0ee107dd91ad419913f1c9.mp4" type="video/mp4" />
                    </video>
                    <div class="w-full font-bold text-xl text-gray-900 px-6">@lang('dress-code.shorter.title')</div>
                    <p class="text-gray-800 text-base px-6 mb-5">@lang('dress-code.shorter.description', ['important' => '<span class="font-bold">'.trans('dress-code.shorter.important').'</span>'])</p>
                    <ul class="w-max space-y-1 text-gray-800 text-base text-gray-500 list-inside dark:text-gray-400 px-6 mb-5">
                        <li class="flex items-center">
                            <svg class="w-5 h-5 mr-1.5 text-green-500 dark:text-green-400 flex-shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                            <x-external-link href="https://www.jjshouse.com/nl/Collection/black-tie-jurken-voor-bruiloftsgasten">@lang('dress-code.examples.black-tie')</x-external-link>
                        </li>
                        <li class="flex items-center">
                            <svg class="w-5 h-5 mr-1.5 text-green-500 dark:text-green-400 flex-shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                            <x-external-link href="https://www.jjshouse.com/nl/Collection/SummerWeddingGuestDresses/Asymmetrische_p10369i5925/Enkellengte_p10369i10370/Vloer-Lengte_p10369i10374/Knie-Lengte_p10369i10375/Sweep-Brush-Train_p10369i10378/Thee-Lengte_p10369i10379/">@lang('dress-code.examples.summer-wedding')</x-external-link>
                        </li>
                        <li class="flex items-center">
                            <svg class="w-5 h-5 mr-1.5 text-gray-400 flex-shrink-0 text-red-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd"></path></svg>
                            <x-external-link href="https://www.jjshouse.com/nl/Collection/SummerWeddingGuestDresses/Kort-Mini_p10369i5921/">@lang('dress-code.examples.too-short')</x-external-link>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <x-base-modal id="warningModal" show="warning">
            <div class="p-4">
                <h2 class="pb-2">@lang('dress-code.warning.title')</h2>
                <p>@lang('dress-code.warning.description')</p>
            </div>
        </x-base-modal>

        <x-base-modal id="imageModal" show="modalOpen" width="">
            <img src="" :src="url" class="h-fu ll rounded-t" style="max-height: calc(100vh - 3rem)" alt="Enlarged Image">
        </x-base-modal>
    </div>
</div>
