@php
    use App\Enums\GuestType;use App\Models\User;
@endphp
<div>
    @auth
        @php
            $timeSlots = User::current()->getTimeSlots();
            $showDays = User::current()->person->getGuestType() !== GuestType::Party
        @endphp
        <div class="h-24"></div>
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg" x-data="{}">
            <table>
                <thead>
                <tr>
                    @if($showDays)
                        <th scope="col">
                            Day
                        </th>
                    @endif
                    <th scope="col">
                        Time Slot
                    </th>
                    <th scope="col">
                        Event
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($timeSlots as $timeSlot)
                    <tr @class(['cursor-pointer' => $timeSlot->hasDescription()]) @click="document.getElementById('{{ $timeSlot->value }}').scrollIntoView({behavior: 'smooth'})">
                        @if($showDays)
                            <td>{{ $timeSlot->getDay() }}</td>
                        @endif
                        <td>{{ $timeSlot->getTimeOnly() }}</td>
                        <td>@lang('planning.labels.'.$timeSlot->value)</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @foreach($timeSlots as $timeSlot)
            @continue(!$timeSlot->hasDescription())
            <div class="flex h-full bg-white rounded-b overflow-hidden shadow-md my-10">
                <div class="w-full flex flex-col flex-grow flex-shrink">
                    <div class="flex-1 bg-white overflow-hidden shadow-md p-6">
                        <h2 id="{{ $timeSlot->value }}">@lang('planning.labels.'.$timeSlot->value)</h2>
                        @foreach(trans('planning.descriptions.'.$timeSlot->value) as $paragraph)
                            <p class="pt-2">{{ $paragraph }}</p>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    @endauth
</div>
