@php use App\Enums\DietType;use App\Enums\Person;use App\Enums\TimeSlot;use App\Models\Rsvp;use App\Models\User;use Illuminate\Support\Facades\DB; @endphp
<x-app-layout>
    <div class="w-full text-gray-800 leading-normal shadow-md sm:rounded-lg mt-24">
        <h2>@lang('navigation.overview')</h2>
        <table class="text-center">
            <tr>
                <th><a href="#all-guests">@lang('rsvp.total')</a></th>
                <td>{{ Rsvp::comingCount() }}</td>
            </tr>
            <tr>
                <th><a href="#all-guests">@lang('rsvp.reacted')</a></th>
                <td><sup title="Expected">{{ Rsvp::count() }}</sup>/<sub title="Total Invited">{{ Rsvp::totalInvited() }}</sub></td>
            </tr>
            <tr>
                <th><a href="#all-guests">@lang('navigation.missing')</a></th>
                <td><sup title="Expected">{{ count(Person::missing(true)) }}</sup>/<sub title="Total Invited">{{ Rsvp::totalInvited() }}</sub></td>
            </tr>
            <tr>
                <th>@lang('rsvp.expected')</a></th>
                <td><sup title="Expected">{{ Rsvp::expected() }}</sup>/<sub title="Total Invited">{{ Rsvp::totalInvited() }}</sub></td>
            </tr>
            <tr>
                <th><a href="#without-rsvp">@lang('rsvp.user-without-rsvp')</a></th>
                <td>{{ User::withoutRsvpCount() }}</td>
            </tr>
            <tr>
                <th><a href="#party-only">@lang('rsvp.party-only')</a></th>
                <td>{{ Rsvp::partyOnlyCount() }}</td>
            </tr>
            <tr>
                <th><a href="#not-coming">@lang('rsvp.not-coming')</a></th>
                <td>{{ Rsvp::notComingCount() }}</td>
            </tr>
            @foreach(TimeSlot::cases() as $timeSlot)
                <tr>
                    <th><a href="#{{ $timeSlot->value }}">@lang('planning.labels.'.$timeSlot->value)</a></th>
                    <td>{{ DB::selectOne('SELECT COUNT(*) AS count FROM rsvps WHERE '.$timeSlot->value.' = true')->count }}</td>
                </tr>
            @endforeach
            <tr>
                <th>@lang('rsvp.allergens')</th>
                <td>
                    <ul class="[&>*:not(:last-child)]:border-b-2">
                        @foreach(Rsvp::whereNotNull('allergens')->getModels() as $rsvp)
                            <li>
                                <a href="{{ url(route('rsvp.'.($rsvp->plusOne ? 'plusOne' : 'control'), $rsvp->person)) }}">{{ $rsvp->name }}</a>
                                <p class="whitespace-pre-wrap">{{ $rsvp->allergens }}</p>
                            </li>
                        @endforeach
                    </ul>
                </td>
            </tr>
            <tr>
                <th>@lang('rsvp.diet-label')</th>
                <td>
                    <div class="grid grid-cols-2">
                        @foreach(DietType::cases() as $dietType)
                            <div>@lang('rsvp.diet-types.'.$dietType->value)</div>
                            <div>{{ DB::selectOne('SELECT COUNT(*) AS count FROM rsvps WHERE dietType = ?', [$dietType->value])->count }}</div>
                        @endforeach
                    </div>
                </td>
            </tr>
            <tr>
                <th>@lang('rsvp.diet')</th>
                <td>
                    <ul class="[&>*:not(:last-child)]:border-b-2">
                        @foreach(Rsvp::whereNotNull('diet')->getModels() as $rsvp)
                            <li>
                                <a href="{{ url(route('rsvp.'.($rsvp->plusOne ? 'plusOne' : 'control'), $rsvp->person)) }}">{{ $rsvp->name }}</a>
                                <p class="whitespace-pre-wrap">{{ $rsvp->diet }}</p>
                            </li>
                        @endforeach
                    </ul>
                </td>
            </tr>
        </table>
    </div>
    @if(DB::selectOne('SELECT COUNT(*) AS count FROM rsvps WHERE dietType = ?', [DietType::Vegetarian->value])->count > 0)
        <div class="w-full text-gray-800 leading-normal shadow-md sm:rounded-lg mt-24">
            <h2>@lang('rsvp.diet-types.'.DietType::Vegetarian->value)</h2>
            <div class="p-8">
                <ul class="text-center">
                    @foreach(Rsvp::where('dietType', DietType::Vegetarian->value)->getModels() as $rsvp)
                        <li><a href="{{ url(route('rsvp.'.($rsvp->plusOne ? 'plusOne' : 'control'), $rsvp->person)) }}">{{ $rsvp->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    @if(DB::selectOne('SELECT COUNT(*) AS count FROM rsvps WHERE dietType = ?', [DietType::Vegan->value])->count > 0)
        <div class="w-full text-gray-800 leading-normal shadow-md sm:rounded-lg mt-24">
            <h2>@lang('rsvp.diet-types.'.DietType::Vegan->value)</h2>
            <div class="p-8">
                <ul class="text-center">
                    @foreach(Rsvp::where('dietType', DietType::Vegan->value)->getModels() as $rsvp)
                        <li><a href="{{ url(route('rsvp.'.($rsvp->plusOne ? 'plusOne' : 'control'), $rsvp->person)) }}">{{ $rsvp->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    @foreach(TimeSlot::cases() as $timeSlot)
        @if(DB::selectOne('SELECT COUNT(*) AS count FROM rsvps WHERE '.$timeSlot->value.' = true')->count > 0)
            <div class="w-full text-gray-800 leading-normal shadow-md sm:rounded-lg mt-24">
                <h2 id="{{ $timeSlot->value }}">@lang('planning.labels.'.$timeSlot->value)</h2>
                <div class="p-8">
                    <ul class="text-center">
                        @foreach(Rsvp::where($timeSlot->value, true)->getModels() as $rsvp)
                            <li><a href="{{ url(route('rsvp.'.($rsvp->plusOne ? 'plusOne' : 'control'), $rsvp->person)) }}">{{ $rsvp->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
    @endforeach
    @if(Rsvp::comingCount() > 0)
        <div class="w-full text-gray-800 leading-normal shadow-md sm:rounded-lg mt-24">
            <h2 id="all-guests">@lang('rsvp.total')</h2>
            <div class="p-8">
                <ul class="text-center">
                    @foreach(Rsvp::coming() as $rsvp)
                        <li><a href="{{ url(route('rsvp.'.($rsvp->plusOne ? 'plusOne' : 'control'), $rsvp->person)) }}">{{ $rsvp->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    @if(Rsvp::partyOnlyCount() > 0)
        <div class="w-full text-gray-800 leading-normal shadow-md sm:rounded-lg mt-24">
            <h2 id="party-only">@lang('rsvp.party-only')</h2>
            <div class="p-8">
                <ul class="text-center">
                    @foreach(Rsvp::partyOnly() as $rsvp)
                        <li><a href="{{ url(route('rsvp.'.($rsvp->plusOne ? 'plusOne' : 'control'), $rsvp->person)) }}">{{ $rsvp->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    @if(Rsvp::notComingCount() > 0)
        <div class="w-full text-gray-800 leading-normal shadow-md sm:rounded-lg mt-24">
            <h2 id="not-coming">@lang('rsvp.not-coming')</h2>
            <div class="p-8">
                <ul class="text-center">
                    @foreach(Rsvp::notComing() as $rsvp)
                        <li>
                            <a href="{{ url(route('rsvp.'.($rsvp->plusOne ? 'plusOne' : 'control'), $rsvp->person)) }}">{{ $rsvp->name }}</a>
                            @if($rsvp->getUser()?->purchases->count() > 0)
                                <span class="text-green-500">✓</span>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    @if(User::withoutRsvpCount() > 0)
        <div class="w-full text-gray-800 leading-normal shadow-md sm:rounded-lg mt-24">
            <h2 id="without-rsvp">@lang('rsvp.user-without-rsvp')</h2>
            <div class="p-8">
                <ul class="text-center">
                    @foreach(User::withoutRsvp() as $user)
                        <li><a href="mailto:{{ $user->email }}">{{ $user->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
</x-app-layout>
