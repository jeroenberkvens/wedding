<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('/site.webmanifest') }}">
        <link rel="mask-icon" href="{{ asset('/safari-pinned-tab.svg') }}" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#00aba9">
        <meta name="theme-color" content="#ffffff">
        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet"/>
        <!-- Styles -->
        <!--suppress CssUnusedSymbol -->
        <style>
            .hidden {
                display: none;
            }
        </style>
        @livewireStyles
        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body class="font-sans antialiased">
    @if (isset($header))
        <header class="bg-white shadow">
            <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                {{ $header }}
            </div>
        </header>
    @else
        <div class="w-full m-0 p-0 bg-cover bg-bottom" style="background-image:url('{{ asset('images/location.jpg') }}'); height: 60vh; max-height:460px;">
            <div class="container max-w-4xl mx-auto pt-16 md:pt-32 text-center break-normal">
                <p class="text-white font-extrabold text-3xl md:text-4xl lg:text-5xl bg-gray-900 bg-opacity-50 pb-6 md:pb-0">
                    <span class="flex justify-center w-full md:inline">
                        <x-rings class="h-32 w-32 mx-2 stroke-white md:inline fill-white"/>
                    </span>
                    <span>@lang('home.header')</span>
                </p>
            </div>
        </div>
    @endif
    <div class="container px-4 md:px-0 max-w-6xl mx-auto -mt-32">
        <div class="mx-0 sm:mx-6">
            <livewire:main-menu/>
            <main>{{ $slot }}</main>
            <x-event-planner-card/>
        </div>
    </div>
    <x-navigation-footer/>
    @livewireScripts
    <script>
        window.warningViewed = () => {
            window.localStorage.setItem('color-warning', 'shown');
            const token = document.querySelector('meta[name="csrf-token"]').content;
            const xmlHttp = new XMLHttpRequest();
            xmlHttp.open('PUT', '{{ route('set-warning-shown') }}');
            xmlHttp.setRequestHeader('X-CSRF-TOKEN', token);
            xmlHttp.send();
        };
        @if(\App\Models\User::tryCurrent()?->warn)
        if (window.localStorage.getItem('color-warning') === 'shown') {
            // User is signed in, and doesn't know yet that there has already been warned, but the localStorage does know that it has already been shown => updating session and user
            window.warningViewed();
        }
        @endif
    </script>
    <x-toaster-hub />
    </body>
</html>
