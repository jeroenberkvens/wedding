<x-app-layout>
    <div class="w-full text-xl md:text-2xl text-gray-800 leading-normal rounded-t">
        <div class="flex h-full bg-white rounded-b overflow-hidden shadow-md">
            <div class="w-full md:w-2/3">
                <img src="{{ asset('images/dinner-table.jpg') }}" class="h-full w-full shadow object-cover" alt="Dinner Table">
            </div>
            <div class="w-full md:w-1/3 flex flex-col flex-grow flex-shrink">
                <div class="flex-1 bg-white overflow-hidden shadow-md">
                    <div class="w-full font-bold text-xl text-gray-900 pt-6 px-6">@lang('home.welcome.title')</div>
                    @foreach(trans('home.welcome.body') as $paragraph)
                        <p class="text-gray-800 font-serif text-base px-6 mb-5">{{ $paragraph }}</p>
                    @endforeach
                    <div class="hidden lg:block mb-5">
                        <h3 class="w-full font-bold text-xl text-gray-900 px-6">@lang('home.general.title')</h3>
                        <p class="text-gray-800 font-serif text-base px-6">@lang('rsvp.notice')</p>
                        <p class="text-gray-800 font-serif text-base px-6">@lang('home.general.body')</p>
                    </div>
                    <div class="hidden md:block mb-5">
                        <h3 class="w-full font-bold text-xl text-gray-900 px-6">@lang('home.location.title')</h3>
                        <p class="text-gray-800 font-serif text-base px-6">@lang('home.location.body')</p>
                        <ul class="ml-8 list-disc text-gray-800 font-serif text-base px-6">
                            <li><x-external-link href="https://goo.gl/maps/DdicQnv39VZS9Z1p8?coh=178573&entry=tt">Google Maps</x-external-link></li>
                            <li><x-external-link href="https://www.google.com/maps/dir//groot+warnsborn/">@lang('home.location.directions')</x-external-link></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-span-2 mt-12 lg:hidden">
            <div class="rounded w-full h-full flex flex-col flex-grow flex-shrink shadow-lg pb-5">
                <h3 class="w-full font-bold text-xl text-gray-900 pt-6 px-6">@lang('home.general.title')</h3>
                <p class="text-gray-800 font-serif text-base px-6">@lang('rsvp.notice')</p>
                <p class="text-gray-800 font-serif text-base px-6">@lang('home.general.body')</p>
            </div>
        </div>
        <div class="md:hidden rounded w-full h-full flex flex-col flex-grow flex-shrink shadow-lg pb-5">
            <h3 class="w-full font-bold text-xl text-gray-900 pt-6 px-6">@lang('home.location.title')</h3>
            <p class="text-gray-800 font-serif text-base px-6">@lang('home.location.body')</p>
            <ul class="ml-8 list-disc text-gray-800 font-serif text-base px-6">
                <li><x-external-link href="https://goo.gl/maps/DdicQnv39VZS9Z1p8?coh=178573&entry=tt">Google Maps</x-external-link></li>
                <li><x-external-link href="https://www.google.com/maps/dir//groot+warnsborn/">@lang('home.location.directions')</x-external-link></li>
            </ul>
        </div>
        <div class="flex flex-wrap justify-between pt-12 space-y-6 md:space-y-0 md:space-x-6">
            <a href="{{ route('rsvp') }}" class="rounded w-full md:w-1/3 flex flex-col flex-grow flex-shrink shadow-lg hover:shadow-xl">
                <div class="flex-1 bg-white overflow-hidden">
                    <img src="{{ asset('images/garden.jpg') }}" class="h-64 w-full object-cover rounded-t pb-6" alt="Garden">
                    <div class="w-full font-bold text-xl text-gray-900 px-6">@lang('home.rsvp.header')</div>
                    <p class="text-gray-800 font-serif text-base px-6 mb-5">@lang('home.rsvp.text')</p>
                </div>
                <div class="flex-none mt-auto bg-white rounded-b overflow-hidden p-6">
                    <div class="flex items-center justify-between">
                        <x-rings class="h-12 w-12 mx-2 stroke-black inline fill-black"/>
                        <p class="text-gray-600 text-xs md:text-sm">@lang('navigation.read-more')</p>
                    </div>
                </div>
            </a>
            <a href="{{ route('dress-code') }}" class="rounded w-full md:w-1/3 flex flex-col flex-grow flex-shrink shadow-lg hover:shadow-xl">
                <div class="flex-1 bg-white overflow-hidden">
                    <img src="{{ asset('images/couple.jpg') }}" class="h-64 w-full object-cover rounded-t pb-6" alt="Couple">
                    <div class="w-full font-bold text-xl text-gray-900 px-6">@lang('home.dress-code.header')</div>
                    <p class="text-gray-800 font-serif text-base px-6 mb-5">@lang('home.dress-code.text')</p>
                </div>
                <div class="flex-none mt-auto bg-white rounded-b overflow-hidden p-6">
                    <div class="flex items-center justify-between">
                        <x-rings class="h-12 w-12 mx-2 stroke-black inline fill-black"/>
                        <p class="text-gray-600 text-xs md:text-sm">@lang('navigation.read-more')</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
</x-app-layout>
