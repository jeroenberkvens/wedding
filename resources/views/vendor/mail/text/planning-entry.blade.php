@php
    use App\Enums\TimeSlot;
    /** @var TimeSlot $timeSlot */
@endphp
@if($checked) +@else -@endif @lang('planning.labels.'.$timeSlot->value)
