@php use App\Models\Gift;use App\Models\Purchase; @endphp
<x-app-layout>
    <div>
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-24" x-data="{}">
            <table class="text-center">
                <thead>
                <tr>
                    <th scope="col" colspan="2">@lang('gift-registry.headers.item')</th>
                    <th scope="col"><sup>{{ Purchase::sum('purchases.amount') }}</sup>/<sub>{{ Gift::sum('gifts.amount') }}</sub></th>
                </tr>
                </thead>
                <tbody>
                @foreach(Gift::withoutPurchasedAndFullyPurchased() as $gift)
                    <tr>
                        <td class="p-0" style="width: 80px">
                            <img class="w-20 inline" src="{{ url($gift->image) }}" alt="gift preview"/>
                        </td>
                        <td class="py-0">
                            <x-external-link href="{{ $gift->url }}"><span>@lang('gift-registry.items.'.$gift->label)</span></x-external-link>
                        </td>
                        <td @class(['text-green-500' => $gift->getPurchaseCount() === $gift->amount])><sup>{{ $gift->getPurchaseCount() }}</sup>/<sub>{{ $gift->amount }}</sub></td>
                    </tr>
                @endforeach
                @foreach(Gift::fullyPurchasedWithoutPurchased() as $gift)
                    <tr>
                        <td class="p-0" style="width: 80px">
                            <img class="w-20 inline" src="{{ url($gift->image) }}" alt="gift preview"/>
                        </td>
                        <td class="py-0">
                            <x-external-link href="{{ $gift->url }}"><span>@lang('gift-registry.items.'.$gift->label)</span></x-external-link>
                        </td>
                        <td @class(['text-green-500' => $gift->getPurchaseCount() === $gift->amount])><sup>{{ $gift->getPurchaseCount() }}</sup>/<sub>{{ $gift->amount }}</sub></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>
