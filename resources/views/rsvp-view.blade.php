@php
    use App\Enums\GuestType;use App\Enums\Person;
    /** @var Person $currentPerson */
    /** @var Person $selectedPerson */
@endphp
<x-app-layout>
    <nav class="mt-20 relative min-h-10" x-data="{ open: false }">
        <div class="flex items-center md:hidden absolute right-0">
            <button @click="open = ! open" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                    <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"/>
                    <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"/>
                </svg>
            </button>
        </div>
        <ul :class="{'block': open, 'hidden md:flex': ! open}" class="grow flex flex-col md:flex-row md:flex-wrap justify-center w-full mb-6 text-sm font-medium text-center text-gray-500 border-b border-gray-200">
            @foreach($currentPerson->controls() as $person)
                @continue(!$person->rsvp()->exists)
                <x-controls-tab :person="$person" :selectedPerson="$selectedPerson" :active="$selectedPerson === $person && !$routeIsPlusOne"/>
                @if($person->plusOneRsvp()->exists)
                    <x-plus-one-tab :person="$person" :selectedPerson="$selectedPerson" :active="$selectedPerson === $person && $routeIsPlusOne"/>
                @endif
            @endforeach
        </ul>
    </nav>
    <div class="w-full text-gray-800 leading-normal rounded-t">
        <table class="text-center">
            @foreach($rsvp->person->getGuestType()->getTimeSlots() as $timeSlot)
                @if($rsvp->{$timeSlot->value})
                    <tr>
                        <th>@lang('planning.labels.'.$timeSlot->value)</th>
                        <td><span class="text-green-500">✓</span></td>
                    </tr>
                @else
                    <tr>
                        <th>@lang('planning.labels.'.$timeSlot->value)</th>
                        <td><span class="text-red-500">✗</span></td>
                    </tr>
                @endif
            @endforeach
            <tr>
                <th>@lang('rsvp.allergens')</th>
                <td>{{ $rsvp->allergens ?? trans('rsvp.none') }}</td>
            </tr>
            <tr>
                <th>@lang('rsvp.diet-label')</th>
                <td>@lang('rsvp.diet-types.'.$rsvp->dietType->value)</td>
            </tr>
            <tr>
                <th>@lang('rsvp.diet')</th>
                <td>{{ $rsvp->diet ?? trans('rsvp.none') }}</td>
            </tr>
        </table>
    </div>
</x-app-layout>
