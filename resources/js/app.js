import './bootstrap';

import Alpine from 'alpinejs';
import focus from '@alpinejs/focus';
import Toaster from '../../vendor/masmerise/livewire-toaster/resources/js';
Alpine.plugin(Toaster);

window.Alpine = Alpine;

Alpine.plugin(focus);

// window.localStorage.removeItem('color-warning');
Alpine.data('dressCode', (url, initialShowWarning = true) => {
    if (initialShowWarning && window.localStorage.getItem('color-warning') === 'shown') {
        // The session is not aware that the user has seen the warning, but the localStorage does => updating the session
        window.warningViewed();
        initialShowWarning = false;
    }
    const object = {
        url: url,
        modalOpen: false,
        warning: initialShowWarning,
    };
    if (initialShowWarning) {
        setTimeout(() => {
            if (object.warning) {
                window.warningViewed();
            }
        }, 2000);
    }
    return object;
});


Alpine.start();
