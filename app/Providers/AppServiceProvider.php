<?php

namespace App\Providers;

use App\Jobs\SendRsvpUpdate;
use App\Models\Rsvp;
use App\Models\User;
use Illuminate\Support\ServiceProvider;
use Sentry\ClientBuilderInterface;
use Sentry\Serializer\Serializer;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->extend(ClientBuilderInterface::class, function (ClientBuilderInterface $clientBuilder) {
            $clientBuilder->setSerializer(new Serializer($clientBuilder->getOptions(), 15));
            return $clientBuilder;
        });
    }

    public function boot(): void
    {
        Rsvp::saved(function (Rsvp $rsvp) {
            $found = User::whereName($rsvp->person->value)->first();
            if ($found === null || $found->isCurrentUser()) {
                return;
            }
            SendRsvpUpdate::schedule(User::current(), $found);
        });
    }
}
