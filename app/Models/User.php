<?php

namespace App\Models;

use App\Enums\Locale;
use App\Enums\Person;
use App\Enums\TimeSlot;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Laravel\Jetstream\HasProfilePhoto;
use Sentry\Serializer\SerializableInterface;
use Webmozart\Assert\Assert;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Locale $locale
 * @property bool $warn
 * @property string|null $remember_token
 * @property string|null $profile_photo_path
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Person $person
 * @property-read DatabaseNotificationCollection<int, DatabaseNotification> $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection<int, \App\Models\Purchase> $purchases
 * @property-read int|null $purchases_count
 * @method static \Database\Factories\UserFactory factory($count = null, $state = [])
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereLocale($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User whereProfilePhotoPath($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static Builder|User whereWarn($value)
 * @mixin Eloquent
 */
class User extends Authenticatable implements SerializableInterface
{
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'locale',
    ];
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'remember_token',
    ];
    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'warn'   => 'bool',
        'person' => Person::class,
        'locale' => Locale::class,
    ];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array<int, string>
     */
    protected $appends = [
        'profile_photo_url',
    ];

    public static function current(): User
    {
        $user = self::tryCurrent();
        Assert::notNull($user, 'No current user');
        return $user;
    }

    public static function tryCurrent(): ?User
    {
        /** @var User $user */
        $user = Auth::user();
        Assert::nullOrIsInstanceOf($user, User::class);
        return $user;
    }

    public static function withoutRsvpCount(): int
    {
        $builder     = User::leftJoin('rsvps', 'users.name', '=', 'rsvps.person')->select('users.*')->whereNotIn('users.name', [Person::JeroenBerkvens->value, Person::JennekeHeere->value])->whereNull('rsvps.id');
        return $builder->count();
    }

    /**
     * @return list<self>
     */
    public static function withoutRsvp(): array
    {
        $builder     = User::leftJoin('rsvps', 'users.name', '=', 'rsvps.person')->select('users.*')->whereNotIn('users.name', [Person::JeroenBerkvens->value, Person::JennekeHeere->value])->whereNull('rsvps.id');
        return $builder->getModels();
    }

    public function isCurrentUser(): bool
    {
        return $this->id = self::tryCurrent()?->id;
    }

    public function __get($key): mixed
    {
        return match ($key) {
            'person' => Person::from($this->name),
            default  => parent::__get($key),
        };
    }

    public function purchases(): HasMany
    {
        return $this->hasMany(Purchase::class);
    }

    /**
     * @return array<TimeSlot>
     */
    public function getTimeSlots(): array
    {
        $timeSlots = [];
        foreach ($this->person->getGuestType()->getTimeSlots() as $timeSlot) {
            $timeSlotName = $timeSlot->value;
            if ($this->person->rsvp()->$timeSlotName) {
                $timeSlots[] = $timeSlot;
            }
        }
        return $timeSlots;
    }

    public function toSentry(): ?array
    {
        return $this->toArray();
    }
}
