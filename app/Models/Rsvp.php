<?php
declare(strict_types=1);

namespace App\Models;

use App\Enums\DietType;
use App\Enums\Person;
use BeTo\Laravel\Models\BaseModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Carbon;

/**
 * App\Models\Rsvp
 *
 * @property int $id
 * @property Person $person
 * @property bool $plusOne
 * @property string $name
 * @property string|null $allergens
 * @property DietType $dietType
 * @property string|null $diet
 * @property bool $lunch
 * @property bool $ceremony
 * @property bool $speech
 * @property bool $reception
 * @property bool $dinerClose
 * @property bool $dinerBistro
 * @property bool $party
 * @property bool $breakfast
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Rsvp newModelQuery()
 * @method static Builder|Rsvp newQuery()
 * @method static Builder|Rsvp query()
 * @method static Builder|Rsvp whereAllergens($value)
 * @method static Builder|Rsvp whereBreakfast($value)
 * @method static Builder|Rsvp whereCeremony($value)
 * @method static Builder|Rsvp whereCreatedAt($value)
 * @method static Builder|Rsvp whereDiet($value)
 * @method static Builder|Rsvp whereDietType($value)
 * @method static Builder|Rsvp whereDinerBistro($value)
 * @method static Builder|Rsvp whereDinerClose($value)
 * @method static Builder|Rsvp whereId($value)
 * @method static Builder|Rsvp whereLunch($value)
 * @method static Builder|Rsvp whereName($value)
 * @method static Builder|Rsvp whereParty($value)
 * @method static Builder|Rsvp wherePerson($value)
 * @method static Builder|Rsvp wherePlusOne($value)
 * @method static Builder|Rsvp whereReception($value)
 * @method static Builder|Rsvp whereSpeech($value)
 * @method static Builder|Rsvp whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class Rsvp extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        'person',
        'plusOne',
        'name',
        'allergens',
        'dietType',
        'diet',
        'lunch',
        'ceremony',
        'speech',
        'reception',
        'dinerClose',
        'dinerBistro',
        'party',
        'breakfast',
    ];
    protected $casts = [
        'person'      => Person::class,
        'dietType'    => DietType::class,
        'plusOne'     => 'boolean',
        'lunch'       => 'boolean',
        'ceremony'    => 'boolean',
        'speech'      => 'boolean',
        'reception'   => 'boolean',
        'dinerClose'  => 'boolean',
        'dinerBistro' => 'boolean',
        'party'       => 'boolean',
        'breakfast'   => 'boolean',
    ];

    /**
     * @return list<self>
     */
    public static function notComing(bool $includePlusOne = true): array
    {
        $query = self::where('lunch', '=', false)
            ->where('ceremony', '=', false)
            ->where('speech', '=', false)
            ->where('reception', '=', false)
            ->where('dinerClose', '=', false)
            ->where('dinerBistro', '=', false)
            ->where('party', '=', false)
            ->where('breakfast', '=', false)
        ;
        if (!$includePlusOne) {
            $query->where('plusOne', '=', false);
        }
        return $query->getModels();
    }

    public static function notComingCount(bool $includePlusOne = true): int
    {
        $notComingPeople = [];
        foreach (self::notComing($includePlusOne) as $rsvp) {
            $notComingPeople[] = $rsvp->person;
        }
        return Person::count($notComingPeople, $includePlusOne);
    }

    private static function comingQuery(bool $includePlusOne): Builder
    {
        $query = self::where(function ($query) {
            $query
                ->where('lunch', '=', true)
                ->orWhere('ceremony', '=', true)
                ->orWhere('speech', '=', true)
                ->orWhere('reception', '=', true)
                ->orWhere('dinerClose', '=', true)
                ->orWhere('dinerBistro', '=', true)
                ->orWhere('party', '=', true)
                ->orWhere('breakfast', '=', true)
            ;
        });
        if (!$includePlusOne) {
            $query->where('plusOne', '=', false);
        }
        return $query;
    }

    /**
     * @return array<self>
     */
    public static function coming(bool $includePlusOne = true): array
    {
        return self::comingQuery($includePlusOne)->getModels();
    }

    public static function comingCount(bool $includePlusOne = true): int
    {
        return self::comingQuery($includePlusOne)->count();
    }

    public static function expected(): int
    {
        $coming           = self::comingCount();
        $notComing        = self::notComingCount();
        $total            = $coming + $notComing;
        $comingPercentage = $coming / $total;
        $totalInvited     = self::totalInvited();
        $totalExpected = $totalInvited * $comingPercentage;
        return (int) round($totalExpected);
    }

    public static function totalInvited(): int
    {
        return Person::count();
    }

    private static function partyOnlyQuery(): Builder
    {
        return self
            ::where('lunch', '=', false)
            ->where('ceremony', '=', false)
            ->where('speech', '=', false)
            ->where('reception', '=', false)
            ->where('dinerClose', '=', false)
            ->where('dinerBistro', '=', false)
            ->where('party', '=', true)
            ->where('breakfast', '=', false)
        ;
    }

    public static function partyOnly(): array
    {
        return self::partyOnlyQuery()->getModels();
    }

    public static function partyOnlyCount(): int
    {
        return self::partyOnlyQuery()->count();
    }

    public function getUser(): ?User
    {
        return User::whereName($this->person->value)->first();
    }

    public function isComing(): bool
    {
        return (
            $this->exists
            && (
                $this->lunch
                || $this->ceremony
                || $this->speech
                || $this->reception
                || $this->dinerClose
                || $this->dinerBistro
                || $this->party
                || $this->breakfast
            )
        );
    }
}
