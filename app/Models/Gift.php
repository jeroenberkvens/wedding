<?php
declare(strict_types=1);

namespace App\Models;

use BeTo\Laravel\Models\BaseModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Gift
 *
 * @property int $id
 * @property string $label
 * @property string $url
 * @property string $image
 * @property int $amount
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection<int, \App\Models\Purchase> $purchases
 * @property-read int|null $purchases_count
 * @method static Builder|Gift newModelQuery()
 * @method static Builder|Gift newQuery()
 * @method static Builder|Gift query()
 * @method static Builder|Gift whereAmount($value)
 * @method static Builder|Gift whereCreatedAt($value)
 * @method static Builder|Gift whereId($value)
 * @method static Builder|Gift whereImage($value)
 * @method static Builder|Gift whereLabel($value)
 * @method static Builder|Gift whereUpdatedAt($value)
 * @method static Builder|Gift whereUrl($value)
 * @mixin \Eloquent
 */
final class Gift extends BaseModel
{
    use HasFactory;

    protected $fillable = ['label', 'url', 'image', 'amount'];

    public function purchases(): HasMany
    {
        return $this->hasMany(Purchase::class);
    }

    public function myPurchase(): ?Purchase
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return User::tryCurrent()?->purchases()->where('gift_id', '=', $this->id)->first();
    }

    /**
     * @return list<self>
     */
    public static function myPurchasedGifts(): array
    {
        $gifts = [];
        foreach (User::tryCurrent()?->purchases ?? [] as $purchase) {
            $gifts[] = $purchase->gift;
        }
        return $gifts;
    }

    /**
     * @return list<self>
     */
    public static function withoutPurchasedAndFullyPurchased(): array
    {
        /** @var Builder $builder */
        $giftColumns = ['gifts.id', 'gifts.label', 'gifts.url', 'gifts.image', 'gifts.amount', 'gifts.created_at', 'gifts.updated_at'];
        $builder     = Gift::leftJoin('purchases', 'gifts.id', '=', 'gift_id')->select('gifts.*')->groupBy($giftColumns)->havingRaw('SUM(purchases.amount) IS NULL OR SUM(purchases.amount) < gifts.amount')->orderByRaw('SUM(purchases.amount) DESC');
        return array_filter($builder->getModels(), fn(self $gift) => $gift->myPurchase() === null);
    }

    /**
     * @return list<self>
     */
    public static function fullyPurchasedWithoutPurchased(): array
    {
        /** @var Builder $builder */
        $giftColumns = ['gifts.id', 'gifts.label', 'gifts.url', 'gifts.image', 'gifts.amount', 'gifts.created_at', 'gifts.updated_at'];
        $builder     = Gift::leftJoin('purchases', 'gifts.id', '=', 'gift_id')->select('gifts.*')->groupBy($giftColumns)->havingRaw('SUM(purchases.amount) = gifts.amount')->orderByRaw('SUM(purchases.amount) DESC');
        return array_filter($builder->getModels(), fn(self $gift) => $gift->myPurchase() === null);
    }

    public function getPurchaseCount(): int
    {
        return (int) $this->purchases()->sum('amount');
    }

    public function getRemainingAmount(): int
    {
        $remainingAmount = $this->amount;
        foreach ($this->purchases as $purchase) {
            if (!$purchase->isMine()) {
                $remainingAmount -= $purchase->amount;
            }
        }
        return $remainingAmount;
    }
}
