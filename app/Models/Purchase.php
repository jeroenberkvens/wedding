<?php
declare(strict_types=1);

namespace App\Models;

use BeTo\Laravel\Models\BaseModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\Purchase
 *
 * @property int $id
 * @property int $gift_id
 * @property int $user_id
 * @property int $amount
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read \App\Models\Gift $gift
 * @property-read \App\Models\User $user
 * @method static Builder|Purchase newModelQuery()
 * @method static Builder|Purchase newQuery()
 * @method static Builder|Purchase query()
 * @method static Builder|Purchase whereAmount($value)
 * @method static Builder|Purchase whereCreatedAt($value)
 * @method static Builder|Purchase whereGiftId($value)
 * @method static Builder|Purchase whereId($value)
 * @method static Builder|Purchase whereUpdatedAt($value)
 * @method static Builder|Purchase whereUserId($value)
 * @mixin \Eloquent
 */
final class Purchase extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        'gift_id',
        'user_id',
        'amount',
    ];

    public function gift(): BelongsTo
    {
        return $this->belongsTo(Gift::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function isMine(): bool
    {
        return $this->user_id === User::tryCurrent()?->id;
    }
}
