<?php
declare(strict_types=1);

namespace App\Models;

use BeTo\Laravel\Models\BaseModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Carbon;

/**
 * App\Models\Present
 *
 * @property int $id
 * @property string $label
 * @property int $amount
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Present newModelQuery()
 * @method static Builder|Present newQuery()
 * @method static Builder|Present query()
 * @method static Builder|Present whereAmount($value)
 * @method static Builder|Present whereCreatedAt($value)
 * @method static Builder|Present whereId($value)
 * @method static Builder|Present whereLabel($value)
 * @method static Builder|Present whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class Present extends BaseModel
{
    use HasFactory;
}
