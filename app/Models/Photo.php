<?php
declare(strict_types=1);

namespace App\Models;

use BeTo\Laravel\Models\BaseModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Carbon;

/**
 * App\Models\Photo
 *
 * @property int $id
 * @property string $path
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Photo newModelQuery()
 * @method static Builder|Photo newQuery()
 * @method static Builder|Photo query()
 * @method static Builder|Photo whereCreatedAt($value)
 * @method static Builder|Photo whereId($value)
 * @method static Builder|Photo wherePath($value)
 * @method static Builder|Photo whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class Photo extends BaseModel
{
    use HasFactory;
}
