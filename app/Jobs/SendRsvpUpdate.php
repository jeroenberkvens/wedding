<?php
declare(strict_types=1);

namespace App\Jobs;

use App\Mail\RsvpUpdated;
use App\Models\User;
use BeTo\Laravel\Exceptions\Handler;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

final class SendRsvpUpdate implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(
        private readonly User $actor,
        public readonly User  $updated,
    ) {
    }

    public static function schedule(User $actor, User $updated): void
    {
        $jobs = DB::table('jobs')->get();
        foreach ($jobs as $jobRow) {
            try {
                $payload = json_decode($jobRow->payload, flags: JSON_THROW_ON_ERROR);
            } catch (\JsonException $exception) {
                Handler::reportBackground($exception);
                continue;
            }
            $job = unserialize($payload->data->command);
            if ($job instanceof SendRsvpUpdate && $job->updated->id === $updated->id) {
                DB::delete('DELETE FROM jobs WHERE id = ?', [$jobRow->id]);
            }
        }
        SendRsvpUpdate::dispatch($actor, $updated)->delay(now()->addMinutes(5));
    }

    public function handle(): void
    {
        Mail::to($this->updated->email)->send(new RsvpUpdated($this->actor, $this->updated));
    }
}
