<?php

namespace App\Actions\Fortify;

use App\Enums\Person;
use App\Models\User;
use BeTo\Laravel\Helpers\EmailRules;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Laravel\Fortify\Contracts\CreatesNewUsers;

final readonly class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * @throws ValidationException
     */
    public function create(array $input): User
    {
        $foundUser = User::whereEmail($input['email'])->first();
        if ($foundUser !== null && $foundUser->exists && $foundUser->name === $input['name']) {
            return $foundUser;
        }
        $rules    = [
            'name'  => ['required', 'string', 'max:255', Rule::in(Person::caseValues())],
            'email' => EmailRules::getUnique(true, includeTeams: false),
        ];
        $messages = [
            'name.required'  => trans('validation.name.required'),
            'name.string'    => trans('validation.name.string'),
            'name.max'       => trans('validation.name.max'),
            'name.in'        => trans('validation.name.in'),
            'email.required' => trans('validation.email.required'),
            'email.string'   => trans('validation.email.something'),
            'email.email'    => trans('validation.email.something'),
            'email.max'      => trans('validation.email.max'),
            'email.unique'   => trans('validation.email.unique'),
        ];
        $data     = Validator::make($input, $rules, $messages)->validate();

        $person             = Person::from($data['name']);
        $data['locale']     = $person->getLocale()->value;
        $data['hasPlusOne'] = $person->hasPlusOne();
        return DB::transaction(function () use ($data) {
            return User::create($data);
        });
    }
}
