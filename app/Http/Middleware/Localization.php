<?php

namespace App\Http\Middleware;

use App\Enums\Locale;
use App\Models\User;
use BeTo\Laravel\Exceptions\Handler;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Sentry\State\Scope;
use Symfony\Component\HttpFoundation\Response;

use function Sentry\configureScope;

final readonly class Localization
{
    public function handle(Request $request, Closure $next): Response
    {
        if (Auth::user() !== null) {
            User::current()->locale->set();
            if (Handler::sentryEnabled()) {
                configureScope(function (Scope $scope): void {
                    $currentUser = User::tryCurrent();
                    if ($currentUser !== null) {
                        $scope->setUser(['id' => $currentUser->id, 'name' => $currentUser->name]);
                    }
                });
            }
        } elseif ($request->session()->has('locale')) {
            Locale::from($request->session()->get('locale'))->set();
        } else {
            $this->setLocaleFromIp($request);
        }
        return $next($request);
    }

    private function setLocaleFromIp(Request $request): void
    {
        $ip = env('FAKE_IP', $request->ip());
        if ($ip === '::1' || $ip === 'localhost') {
            return;
        }
        $ipGeolocationInfo = unserialize(file_get_contents('https://ip-get-geolocation.com/api/serializedphp/' . $ip . '?key=' . env('IP_GEOLOCATION_API_KEY')));
        $country           = $ipGeolocationInfo['country'] ?? 'Netherlands';
        $locale            = $country === 'Netherlands' ? Locale::Dutch : Locale::English;
        $locale->set();
    }
}
