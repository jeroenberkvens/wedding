<?php

namespace App\Http\Middleware;

use App\Models\User;
use BeTo\Laravel\Exceptions\Handler;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Sentry\State\Scope;

use function Sentry\configureScope;

final class Authenticate extends Middleware
{
    public function handle($request, \Closure $next, ...$guards)
    {
        if (Handler::sentryEnabled()) {
            configureScope(function (Scope $scope): void {
                $currentUser = User::tryCurrent();
                if ($currentUser !== null) {
                    $scope->setUser(['id' => $currentUser->id, 'name' => $currentUser->name]);
                }
            });
        }
        return parent::handle($request, $next, ...$guards);
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     */
    protected function redirectTo(Request $request): ?string
    {
        return $request->expectsJson() ? null : route('home');
    }
}
