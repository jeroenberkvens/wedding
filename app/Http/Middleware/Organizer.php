<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use App\Enums\GuestType;
use App\Models\User;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

final class Organizer
{
    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next): Response|RedirectResponse
    {
        if (User::tryCurrent()?->person->getGuestType() === GuestType::Organizers) {
            return $next($request);
        }
        return redirect(route('home'));
    }
}
