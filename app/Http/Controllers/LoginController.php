<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\User;
use BeTo\Laravel\Helpers\EmailRules;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

final class LoginController extends Controller
{
    /**
     * @throws ValidationException
     */
    public function login(Request $request): RedirectResponse
    {
        $messages = [
            'required' => trans('validation.email.required'),
            'string'   => trans('validation.email.something'),
            'email'    => trans('validation.email.something'),
            'max'      => trans('validation.email.max'),
            'exists'   => trans('validation.email.exists'),
        ];
        $data     = $this->validate($request, ['email' => EmailRules::getExists('users')], $messages);
        Auth::login(User::whereEmail($data['email'])->firstOrFail());
        $request->session()->regenerate();
        return redirect(route('rsvp'));
    }
}
