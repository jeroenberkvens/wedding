<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Enums\GuestType;
use App\Enums\Person;
use App\Models\User;
use Illuminate\View\View;

final class RsvpController extends Controller
{
    public function rsvp(): View
    {
        $person = User::tryCurrent()?->person;
        return view('rsvp', compact(['person']));
    }

    public function rsvpForPerson(Person $person): View
    {
        $currentPerson = User::tryCurrent()?->person;
        if ($currentPerson?->getGuestType() === GuestType::Organizers) {
            $selectedPerson = $person;
            $rsvp = $person->rsvp();
            $routeIsPlusOne = request()->routeIs('rsvp.plusOne');
            return view('rsvp-view', compact(['rsvp', 'selectedPerson', 'routeIsPlusOne', 'currentPerson']));
        } else {
            return view('rsvp', compact(['person']));
        }
    }
}
