<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Enums\Person;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

final class DressCodeController extends Controller
{
    public function show(): View
    {
        if (Auth::user() !== null) {
            $showWarning = User::current()->warn;
        } elseif (session()->has('warn')) {
            /** @noinspection PhpUnhandledExceptionInspection handled by the has-check above */
            $showWarning = session()->get('warn');
        } else {
            $showWarning = true;
            session()->put('warn', false);
        }
        $showWarning = true;

        return view('dress-code', compact(['showWarning']));
    }
}
