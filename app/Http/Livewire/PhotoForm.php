<?php
declare(strict_types=1);

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Contracts\View\View;

final class PhotoForm extends Component
{
    public function render(): View
    {
        return view('livewire.photo-form');
    }
}
