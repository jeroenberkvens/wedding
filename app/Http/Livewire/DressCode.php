<?php
declare(strict_types=1);

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Illuminate\Contracts\View\View;

final class DressCode extends Component
{
    public bool $showWarning = true;

    public function mount(): void
    {
        if (Auth::user() !== null) {
            $this->showWarning = User::current()->warn;
        } elseif (session()->has('warn')) {
            /** @noinspection PhpUnhandledExceptionInspection handled by the has-check above */
            $this->showWarning = session()->get('warn');
        } else {
            $this->showWarning = true;
        }
    }

    public function render(): View
    {
        return view('livewire.dress-code');
    }
}
