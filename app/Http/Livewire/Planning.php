<?php
declare(strict_types=1);

namespace App\Http\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

final class Planning extends Component
{
    protected $listeners = ['authChange'];

    public function authChange(): void
    {
        $this->render();
    }

    public function render(): View
    {
        return view('livewire.planning');
    }
}
