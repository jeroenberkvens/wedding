<?php
declare(strict_types=1);

namespace App\Http\Livewire;

use App\Enums\GuestType;
use App\Enums\Locale;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

final class MainMenu extends Component
{
    public bool   $register  = false;
    public string $locale;
    protected     $listeners = ['authChange' => 'render'];

    public function mount(): void
    {
        $this->locale = Locale::current()->value;
    }

    public function logout(): void
    {
        Auth::logout();
        session()->invalidate();
        session()->regenerateToken();
        redirect(request()->header('Referer'));
    }

    public function toggleLanguage(): void
    {
        Locale::toggle();
    }

    public function render(): View
    {
        if (User::tryCurrent()?->person->getGuestType() === GuestType::Organizers) {
            return view('livewire.admin-menu');
        }
        return view('livewire.main-menu');
    }
}
