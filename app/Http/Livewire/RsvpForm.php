<?php
declare(strict_types=1);

namespace App\Http\Livewire;

use App\Enums\Person;
use App\Models\Rsvp;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Masmerise\Toaster\Toaster;

final class RsvpForm extends Component
{
    public Person  $currentPerson;
    public Person  $person;
    public Rsvp    $rsvp;
    public string  $name;
    public ?string $allergens;
    public string  $dietType;
    public ?string $diet;
    public bool    $lunch;
    public bool    $ceremony;
    public bool    $speech;
    public bool    $reception;
    public bool    $dinerClose;
    public bool    $dinerBistro;
    public bool    $party;
    public bool    $breakfast;
    public bool    $routeIsRsvp;
    public bool    $routeIsPlusOne;
    public bool    $routeIsControls;
    protected      $listeners = ['authChange'];

    protected function getRules(): array
    {
        return $this->person->getGuestType()->getRules();
    }

    protected function getMessages(): array
    {
        return [
            'name.required'       => trans('validation.name.required'),
            'name.string'         => trans('validation.name.string'),
            'name.regex'          => trans('validation.name.regex'),
            'allergens.string'    => trans('validation.allergens'),
            'dietType.required'   => trans('validation.dietType.required'),
            'dietType.string'     => trans('validation.dietType.string'),
            'diet.string'         => trans('validation.diet'),
            'lunch.boolean'       => trans('validation.planning'),
            'lunch.false'         => trans('validation.planning'),
            'ceremony.boolean'    => trans('validation.planning'),
            'ceremony.false'      => trans('validation.planning'),
            'speech.boolean'      => trans('validation.planning'),
            'speech.false'        => trans('validation.planning'),
            'reception.boolean'   => trans('validation.planning'),
            'reception.false'     => trans('validation.planning'),
            'dinerClose.boolean'  => trans('validation.planning'),
            'dinerClose.false'    => trans('validation.planning'),
            'dinerBistro.boolean' => trans('validation.planning'),
            'dinerBistro.false'   => trans('validation.planning'),
            'party.boolean'       => trans('validation.planning'),
            'party.false'         => trans('validation.planning'),
            'breakfast.boolean'   => trans('validation.planning'),
            'breakfast.false'     => trans('validation.planning'),
        ];
    }

    public function authChange(?User $user): void
    {
        $this->mount($user?->person);
    }

    public function mount(?Person $providedPerson): void
    {
        if (Auth::user() === null) {
            return;
        }
        $this->currentPerson   = User::current()->person;
        $person                = $providedPerson ?? $this->currentPerson;
        $this->routeIsRsvp     = request()->routeIs('rsvp');
        $this->routeIsPlusOne  = request()->routeIs('rsvp.plusOne');
        $this->routeIsControls = request()->routeIs('rsvp.control');
        $this->person          = $person;
        $this->rsvp            = $this->routeIsPlusOne ? $person->plusOneRsvp() : $person->rsvp();
        $this->name            = $this->rsvp->name;
        $this->allergens       = $this->rsvp->allergens;
        $this->dietType        = $this->rsvp->dietType->value;
        $this->diet            = $this->rsvp->diet;
        $this->lunch           = $this->rsvp->lunch;
        $this->ceremony        = $this->rsvp->ceremony;
        $this->speech          = $this->rsvp->speech;
        $this->reception       = $this->rsvp->reception;
        $this->dinerClose      = $this->rsvp->dinerClose ?? false;
        $this->dinerBistro     = $this->rsvp->dinerBistro ?? false;
        $this->party           = $this->rsvp->party;
        $this->breakfast       = $this->rsvp->breakfast;
    }

    /**
     * @throws \Throwable When failing to update or save
     */
    public function submit(): void
    {
        $data = $this->validate();
        foreach (['allergens', 'diet'] as $nullable) {
            if ($data[$nullable] === '') {
                $data[$nullable] = null;
            }
        }
        $data['plusOne'] = $this->routeIsPlusOne;
        if ($this->rsvp->exists) {
            $this->rsvp->updateOrFail($data);
        } else {
            $this->rsvp->fill($data)->saveOrFail();
        }
        Toaster::success(trans('rsvp.updated'));
    }

    public function render(): View
    {
        return view('livewire.rsvp-form');
    }
}
