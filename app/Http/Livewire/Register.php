<?php
declare(strict_types=1);

namespace App\Http\Livewire;

use App\Actions\Fortify\CreateNewUser;
use App\Enums\Person;
use App\Models\Rsvp;
use App\Models\User;
use BeTo\Laravel\Helpers\EmailRules;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Livewire\Component;

final class Register extends Component
{
    public bool   $autoHide  = false;
    public bool   $login     = false;
    public string $name      = '';
    public string $email     = '';
    protected     $listeners = ['authChange' => 'render'];

    public function mount(bool $autoHide = false): void
    {
        $this->autoHide = $autoHide;
    }

    /**
     * @throws ValidationException
     */
    public function registerOrLogin(): void
    {
        if ($this->login) {
            $this->validate(
                ['email' => EmailRules::getExists('users')],
                [
                    'email.required' => trans('validation.email.required'),
                    'email.string'   => trans('validation.email.something'),
                    'email.email'    => trans('validation.email.something'),
                    'email.max'      => trans('validation.email.max'),
                    'email.exists'   => trans('validation.email.exists'),
                ],
            );
            $user = User::whereEmail($this->email)->first();
        } else {
            $rsvp = Rsvp::whereName($this->name)->first();
            $name = $rsvp?->exists ? $rsvp->person->value : $this->name;
            $user = (new CreateNewUser())->create(['name' => $name, 'email' => $this->email]);
        }
        Auth::login($user, true);
        $this->emit('authChange', $user->id);
        $this->dispatchBrowserEvent('authentication-changed', ['user' => $user]);
        $this->render();
    }

    public function render(): View
    {
        $names = [];
        foreach (Person::cases() as $person) {
            if ($person->rsvp()->exists) {
                $names[] = $person->rsvp()->name;
            } else {
                $names[] = $person->value;
            }
        }
        return view('livewire.register', compact('names'));
    }
}
