<?php
declare(strict_types=1);

namespace App\Http\Livewire;

use App\Models\Gift;
use App\Models\Purchase;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Masmerise\Toaster\Toaster;

final class GiftRegistry extends Component
{
    public bool  $showModal    = false;
    public ?Gift $selectedGift = null;
    public int   $amount       = 1;
    protected    $listeners    = ['authChange'];

    protected function getRules(): array
    {
        return [
            'amount' => ['required', 'int', 'min:1', 'max:' . $this->selectedGift->getRemainingAmount()],
        ];
    }

    protected function getMessages(): array
    {
        return [
            'amount.required' => trans('validation.amount.required'),
            'amount.int'      => trans('validation.amount.int'),
            'amount.min'      => trans('validation.amount.min'),
            'amount.max'      => trans('validation.amount.max'),
        ];
    }

    public function authChange(): void
    {
        $this->showModal = true;
        $this->render();
    }

    public function openModal(Gift $gift = null): void
    {
        $this->selectedGift = $gift;
        $this->amount       = $this->selectedGift->myPurchase()?->amount ?? 1;
        $this->showModal    = true;
    }

    public function cancelPurchase(): void
    {
        User::current()->purchases()->where('gift_id', '=', $this->selectedGift->id)->delete();
        $this->showModal = false;
        Toaster::info(trans('gift-registry.notifications.never-mind'));
    }

    public function registerPurchase(): void
    {
        $data = $this->validate();
        Purchase::updateOrCreate(['gift_id' => $this->selectedGift->id, 'user_id' => User::current()->id], $data);
        $this->showModal = false;
        Toaster::success(trans('gift-registry.notifications.thanks'));
    }

    public function render(): View
    {
        return view('livewire.gift-registry');
    }
}
