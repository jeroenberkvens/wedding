<?php
declare(strict_types=1);

namespace App\Enums;

use BeTo\Laravel\Enums\GetCaseValuesTrait;

enum DietType: string
{
    use GetCaseValuesTrait;

    case Normal = 'normal';
    case Vegetarian = 'vegetarian';
    case Vegan = 'vegan';
}
