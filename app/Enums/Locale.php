<?php
declare(strict_types=1);

namespace App\Enums;

use App\Models\User;
use BeTo\Laravel\Enums\GetCaseValuesTrait;
use BeTo\Laravel\Exceptions\Handler;
use Illuminate\Support\Facades\App;
use Sentry\State\Scope;

use function Sentry\configureScope;

enum Locale: string
{
    use GetCaseValuesTrait;

    case English = 'en';
    case Dutch   = 'nl';

    public static function current(): self
    {
        return self::from(App::getLocale());
    }

    public function getOther(): self
    {
        return match ($this) {
            self::English => self::Dutch,
            self::Dutch   => self::English,
        };
    }

    public function set(): void
    {
        User::tryCurrent()?->update(['locale' => $this]);
        session()->put('locale', $this->value);
        App::setLocale($this->value);
        redirect(request()->header('Referer'));

        if (Handler::sentryEnabled()) {
            configureScope(function (Scope $scope): void {
                $scope->setTag('language', $this->value);
            });
        }
    }
    public static function toggle(): void
    {
        Locale::current()->getOther()->set();
    }
}
