<?php
declare(strict_types=1);

namespace App\Enums;

enum SortDirection: string
{
    case Ascending = 'asc';
    case Descending = 'desc';

    public static function get(): self
    {
        return self::from(request()->get('direction', 'asc'));
    }

    public static function getOther(): self
    {
        return match (self::get()) {
            self::Ascending  => self::Descending,
            self::Descending => self::Ascending,
        };
    }
}
