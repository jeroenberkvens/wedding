<?php
declare(strict_types=1);

namespace App\Enums;

enum PersonSort: string
{
    case Name    = 'name';
    case Account = 'account';
    case RSVP    = 'rsvp';
    case PlusOne = 'plus-one';

    public static function get(): self
    {
        return self::from(request()->get('sort', 'name'));
    }

    public function getArgs(): array
    {
        $args = ['sort' => $this];
        if ($this === self::get()) {
            $args['direction'] = SortDirection::getOther();
        }
        return $args;
    }

    public function sort(Person $a, Person $b): int
    {
        if (SortDirection::get() === SortDirection::Descending) {
            $tmp = $a;
            $a   = $b;
            $b   = $tmp;
        }
        return match ($this) {
            self::Name    => $this->sortByName($a, $b),
            self::Account => $this->sortByAccount($a, $b),
            self::RSVP    => $this->sortByRsvp($a, $b),
            self::PlusOne => $this->sortByPlusOne($a, $b),
        };
    }

    private function sortByName(Person $a, Person $b): int
    {
        return ($a->rsvp()->name ?? $a->value) <=> ($b->rsvp()->name ?? $b->value);
    }

    private function sortByAccount(Person $a, Person $b): int
    {
        $sort = $b->hasAccount() <=> $a->hasAccount();
        if ($sort !== 0) {
            return $sort;
        }
        return $this->sortByName($a, $b);
    }

    private function sortByRsvp(Person $a, Person $b): int
    {
        $sort = $b->rsvp()->exists <=> $a->rsvp()->exists;
        if ($sort !== 0) {
            return $sort;
        }
        $sort = $b->rsvp()->isComing() <=> $a->rsvp()->isComing();
        if ($sort !== 0) {
            return $sort;
        }
        return $this->sortByAccount($a, $b);
    }

    private function sortByPlusOne(Person $a, Person $b): int
    {
        $sort = SortDirection::get() === SortDirection::Descending ? $a->hasPlusOne() <=> $b->hasPlusOne() : $b->hasPlusOne() <=> $a->hasPlusOne();
        if ($sort !== 0) {
            return $sort;
        }
        $sort = $b->plusOneRsvp()->exists <=> $a->plusOneRsvp()->exists;
        if ($sort !== 0) {
            return $sort;
        }
        return $this->sortByRsvp($a, $b);
    }
}
