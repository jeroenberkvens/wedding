<?php
declare(strict_types=1);

namespace App\Enums;

enum Gift: string
{
    case Eetbord                   = 'eetbord';
    case UniverseelBord            = 'universeel-bord';
    case Ontbijtbord               = 'ontbijtbord';
    case Broodbord                 = 'broodbord';
    case Gourmetbord               = 'gourmetbord';
    case RechthoekigeServeerschaal = 'rechthoekige-serveerschaal';
    case MultifunctioneelBord      = 'multifunctioneel-bord';
    case Pastabord                 = 'pastabord';
    case Dipschaaltje              = 'dipschaaltje';
    case WitteWijnglas             = 'witte-wijnglas';
    case RodeWijnglas              = 'rode-wijnglas';
    case Champagneglas             = 'champagneglas';
    case Whiskyglas                = 'whiskyglas';
    case PlatteSchaal              = 'platte-schaal';
    case Bol                       = 'bol';
    case Koffieschotel             = 'koffieschotel';
    case EspressoSchotel           = 'espresso-schotel';
    case CafeAuLaitSchotel         = 'cafe-au-lait-schotel';
    case Koffiekop                 = 'koffiekop';
    case BekerL                    = 'beker-l';
    case Espressokopje             = 'espressokopje';
    case BekerS                    = 'beker-s';

    public function getAmountWanted(): int
    {
        return match ($this) {
            self::Eetbord,
            self::BekerL,
            self::Koffiekop,
            self::CafeAuLaitSchotel,
            self::Koffieschotel,
            self::Bol,
            self::PlatteSchaal,
            self::Dipschaaltje,
            self::Gourmetbord,
            self::Broodbord,
            self::Ontbijtbord               => 8,
            self::Pastabord                 => 4,
            self::UniverseelBord,
            self::BekerS,
            self::Espressokopje,
            self::EspressoSchotel,
            self::Whiskyglas,
            self::Champagneglas,
            self::RodeWijnglas,
            self::WitteWijnglas,
            self::MultifunctioneelBord,
            self::RechthoekigeServeerschaal => 2,
        };
    }

    public function getUrl(): string
    {
        return match ($this) {
            self::Eetbord                   => 'https://www.villeroy-boch.nl/shop/manufacture-rock-eetbord-1042392620.html',
            self::UniverseelBord            => 'https://www.villeroy-boch.nl/shop/manufacture-rock-universeel-bord-coupe-25-cm-1042392630.html',
            self::Ontbijtbord               => 'https://www.villeroy-boch.nl/shop/manufacture-rock-ontbijtbord-1042392640.html',
            self::Broodbord                 => 'https://www.villeroy-boch.nl/shop/manufacture-rock-broodbord-1042392660.html',
            self::Gourmetbord               => 'https://www.villeroy-boch.nl/shop/manufacture-rock-gourmetbord-1042392590.html',
            self::RechthoekigeServeerschaal => 'https://www.villeroy-boch.nl/shop/manufacture-rock-rechthoekige-serveerschaal-zwartgrijs-35-x-18-x-1-cm-1042392281.html',
            self::MultifunctioneelBord      => 'https://www.villeroy-boch.nl/shop/manufacture-rock-multifunctioneel-bord-zwartgrijs-28-x-17-x-1-cm-1042392772.html',
            self::Pastabord                 => 'https://www.villeroy-boch.nl/shop/manufacture-rock-pastabord-28x28x5cm-1042392790.html',
            self::Dipschaaltje              => 'https://www.villeroy-boch.nl/shop/manufacture-rock-dipschaaltje-zwartgrijs-8-x-8-x-4-cm-1042393932.html',
            self::WitteWijnglas             => 'https://www.villeroy-boch.nl/shop/manufacture-rock-witte-wijnglas-4-stuks-380-ml-1137988120.html',
            self::RodeWijnglas              => 'https://www.villeroy-boch.nl/shop/manufacture-rock-rode-wijnglas-4-stuks-470-ml-1137988110.html',
            self::Champagneglas             => 'https://www.villeroy-boch.nl/shop/manufacture-rock-champagneglas-4-stuks-260-ml-1137988131.html',
            self::Whiskyglas                => 'https://www.villeroy-boch.nl/shop/manufacture-rock-whiskyglas-250-ml-4-stuks-1137988250.html',
            self::PlatteSchaal              => 'https://www.villeroy-boch.nl/shop/manufacture-rock-glow-platte-schaal-koperzwart-1042832536.html',
            self::Bol                       => 'https://www.villeroy-boch.nl/shop/manufacture-rock-glow-bol-koperzwart-14-x-14-x-7-cm-1042831900.html',
            self::Koffieschotel             => 'https://www.villeroy-boch.nl/shop/manufacture-rock-glow-koffieschotel-koperzwart-155-x-155-x-2-cm-1042831310.html',
            self::EspressoSchotel           => 'https://www.villeroy-boch.nl/shop/manufacture-rock-glow-espresso-schotel-koperzwart-12-x-12-x-2-cm-1042831430.html',
            self::CafeAuLaitSchotel         => 'https://www.villeroy-boch.nl/shop/manufacture-rock-glow-cafe-au-lait-schotel-koperzwart-17-x-17-x-2-cm-1042831220.html',
            self::Koffiekop                 => 'https://www.villeroy-boch.nl/shop/manufacture-rock-koffiekop-zwartgrijs-105-x-8-x-75-cm-1042391300.html',
            self::BekerL                    => 'https://www.villeroy-boch.nl/shop/manufacture-rock-beker-l-300-ml-1042397983.html',
            self::Espressokopje             => 'https://www.villeroy-boch.nl/shop/manufacture-rock-espressokopje-zwartgrijs-85-x-65-x-6-cm-1042391420.html',
            self::BekerS                    => 'https://www.villeroy-boch.nl/shop/manufacture-rock-beker-s-70-ml-1042397982.html',
        };
    }

    public function getImageUrl(): string
    {
        return match ($this) {
            self::Eetbord                   => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dwf885d59d/images/1042392620_on_use-1.jpg?sw=80',
            self::UniverseelBord            => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dw0d54c658/images/1042392630_on_mil-1.jpg?sw=80',
            self::Ontbijtbord               => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dw5c4e5563/images/1042392640_on_mil-1.jpg?sw=80',
            self::Broodbord                 => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dw2e05bf74/images/1042392660_on_mil-1.jpg?sw=80',
            self::Gourmetbord               => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dw96295d64/images/1042392590_on_usp-d-1.jpg?sw=80',
            self::RechthoekigeServeerschaal => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dwa83fad23/images/1042392281_on_use-1.jpg?sw=80',
            self::MultifunctioneelBord      => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dw5141b754/images/1042392772_on_use-1.jpg?sw=80',
            self::Pastabord                 => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dwc1aae590/images/1042392790_on_use-1.jpg?sw=80',
            self::Dipschaaltje              => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dw7f0a6735/images/1042393932_1042392281_1042399023_on_usp-d-1.jpg?sw=80',
            self::WitteWijnglas             => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dwea1d737f/images/1137988110_1137988120_on_mil-2.jpg?sw=80',
            self::RodeWijnglas              => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dw48fd717f/images/1137988110_on_use-1.jpg?sw=80',
            self::Champagneglas             => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dwf09646a0/images/1137988131_on_use-1.jpg?sw=80',
            self::Whiskyglas                => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dw687c532b/images/1137988250_on_mil-1.jpg?sw=80',
            self::PlatteSchaal              => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dw99ece307/images/1042832536_on_mil-1.jpg?sw=80',
            self::Bol                       => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dwe4c4fd65/images/1042831900_on_mil-1.jpg?sw=80',
            self::Koffieschotel             => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dw356a661d/images/1042831310_on_mil-1.jpg?sw=80',
            self::EspressoSchotel           => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dw368ce34e/images/1042831220_1042831430_on_mil-1.jpg?sw=80',
            self::CafeAuLaitSchotel         => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dw7f1586ae/images/1042831220_on_use-1.jpg?sw=80',
            self::Koffiekop                 => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dw5c28bbd9/images/1042391300_1042391310_on_mil-1.jpg?sw=80',
            self::BekerL                    => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dwef15bbc9/images/1042397983_on_fs-1.jpg?sw=80',
            self::Espressokopje             => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dw483d917f/images/1042401300_1042401310_1042401290_1042391411_1042399651_1042391420_1042391290_1042391430_1042409651_1042401420_1042401411_on_coll-1.jpg?sw=80',
            self::BekerS                    => 'https://www.villeroy-boch.nl/dw/image/v2/BCRC_PRD/on/demandware.static/-/Sites-master/default/dw02f1c445/images/1042397982_on_use-1.jpg?sw=80',
        };
    }

    public function update(): void
    {
        \App\Models\Gift::whereLabel($this->value)->update(['url' => $this->getUrl(), 'image' => $this->getImageUrl(), 'amount' => $this->getAmountWanted()]);
    }

    public function create(): void
    {
        \App\Models\Gift::create(['label' => $this->value, 'url' => $this->getUrl(), 'image' => $this->getImageUrl(), 'amount' => $this->getAmountWanted()]);
    }
}
