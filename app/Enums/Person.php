<?php
declare(strict_types=1);

namespace App\Enums;

use App\Models\Rsvp;
use App\Models\User;
use BeTo\Laravel\Enums\GetCaseValuesTrait;

enum Person: string
{
    use GetCaseValuesTrait;

    case JeroenBerkvens          = 'Jeroen Berkvens';
    case JennekeHeere            = 'Jenneke Heere';
    case CarolineVanDenPol       = 'Caroline van den Pol';
    case TheoDuPont              = 'Theo du Pont';
    case NielsHeere              = 'Niels Heere';
    case RickHeere               = 'Rick Heere';
    case MarinusHeere            = 'Marinus (Bob) Heere';
    case AlainDuPont             = 'Alain du Pont';
    case ThomBerkvens            = 'Thom Berkvens';
    case HettyBerkvens           = 'Hetty Berkvens';
    case VictorSliphorst         = 'Victor Sliphorst';
    case EvaSliphorst            = 'Eva Sliphorst';
    case MarianneSliphorst       = 'Marianne Sliphorst';
    case LucSliphorst            = 'Luc Sliphorst';
    case ChantalBerkvens         = 'Chantal Berkvens';
    case RuudStraten             = 'Ruud Straten';
    case AnoukBerkvens           = 'Anouk Berkvens';
    case DaanVanRooij            = 'Daan van Rooij';
    case JoeyVanRooij            = 'Joey van Rooij';
    case RosalieVanRooij         = 'Rosalie van Rooij';
    case MireilleVandenBarselaar = 'Mireille Van den Barselaar';
    case FrankVandenBarselaar    = 'Frank Van den Barselaar';
    case BjornVandenBarselaar    = 'Björn Van den Barselaar';
    case OlivierVandenBarselaar  = 'Olivier Van den Barselaar';
    case CarmenVandenBarselaar   = 'Carmen Van den Barselaar';
    case SaryaVanderKuij         = 'Sarya Van der Kuij';
    case AmberVanderKuij         = 'Amber Van der Kuij';
    case EllenHeere              = 'Ellen Heere';
    case AlbertHogeweij          = 'Albert Hogeweij';
    case KeesVandenPol           = 'Kees Van den Pol';
    case EllisVandenPol          = 'Ellis Van den Pol';
    case RemcoVandenPol          = 'Remco Van den Pol';
    case LeticiaAssuncoaCordeiro = 'Letícia Assuncoa Cordeiro';
    case MarkVandenPol           = 'Mark Van den Pol';
    case MarlieMeulenberg        = 'Marlie Meulenberg';
    case NomiVandenPol           = 'Nomi Van den Pol';
    case GemmaMeuwissen          = 'Gemma Meuwissen';
    case AkuaNielen              = 'Akua Nielen';
    case MaartenNielen           = 'Maarten Nielen';
    case ReinierNielenKaal       = 'Reinier Nielen-Kaal';
    case AnnaNielenKaal          = 'Anna Nielen-Kaal';
    case ErikNielen              = 'Erik Nielen';
    case JasmijnNielen           = 'Jasmijn Nielen';
    case WouterNielen            = 'Wouter Nielen';
    case JudithNowee             = 'Judith Nowee';
    case MarcelNowee             = 'Marcel Nowee';
    case GwenNowee               = 'Gwen Nowee';
    case MaxSnoeijen             = 'Max Snoeijen';
    case MyrteNowee              = 'Myrte Nowee';
    case MaxRoelofs              = 'Max Roelofs';
    case StijnNowee              = 'Stijn Nowee';
    case DiedeVanVleuten         = 'Diede Van Vleuten';
    case RonaldVanGenabeek       = 'Ronald Van Genabeek';
    case IngridVanGenabeek       = 'Ingrid Van Genabeek';
    case LeonieBerkvens          = 'Leonie Berkvens';
    case JohannusBerkvens        = 'Johannus (Hans Christiaan) (HC) Berkvens';
    case CatherinaBerkvens       = 'Catherina (Karin) Berkvens';
    case AnnabelTangel           = 'Annabel Tangel';
    case JosephineBerkvens       = 'Josephine Berkvens';
    case MathildeBerkvens        = 'Mathilde Berkvens';
    case BoukeBarnas             = 'Bouke Barnas';
    case MariaBarnas             = 'Maria (Maaike) Barnas';
    case JohannesBerkvens        = 'Johannes (Sebastiaan) Berkvens';
    case AnnikaBerkvensVisser    = 'Annika Berkvens - Visser';
    case JustBerkvens            = 'Just Berkvens';
    case HiddeBerkvens           = 'Hidde Berkvens';
    case GwenBerkvens            = 'Gwen Berkvens';
    case AnnickBerkvens          = 'Annick Berkvens';
    case SanderTeerling          = 'Sander Teerling';
    case CynthiaTeerling         = 'Cynthia Teerling';
    case ManoahTeerling          = 'Manoah Teerling';
    case RoosTeerling            = 'Roos Teerling';
    case BartFluitsma            = 'Bart Fluitsma';
    case NasimaFluitsma          = 'Nasima Fluitsma';
    case MaaikeLeahe             = 'Maaike Leahe';
    case DominicLeahe            = 'Dominic Leahe';
    case MadeliefLeahe           = 'Madelief Leahe';
    case HamishLeahe             = 'Hamish Leahe';
    case JoostFluitsma           = 'Joost Fluitsma';
    case ChristiaanTeerling      = 'Christiaan Teerling';
    case AnetteTeerling          = 'Anette Teerling';
    case MichielTeerling         = 'Michiel Teerling';
    case HannaTangel             = 'Hanna Tangel';
    case HielkeFluitsma          = 'Hielke Fluitsma';
    case RennekeFluitsma         = 'Renneke Fluitsma';
    case JosephineTeerling       = 'Josephine Teerling';
    case JaimyBaartmans          = 'Jaimy Baartmans';
    case MariskaMuijtjens        = 'Mariska Muijtjens';
    case RemcoMuijtjens          = 'Remco Muijtjens';
    case ElenaMuijtjens          = 'Elena Muijtjens';
    case MarieAnneToonen         = 'Marie-Anne Toonen';
    case NickWanmaker            = 'Nick Wanmaker';
    case ThomasFranken           = 'Thomas (Tom) Franken';
    case AstridBelder            = 'Astrid Belder';
    case NicolienVanDongen       = 'Nicolien Van Dongen';
    case BramHurkmans            = 'Bram Hurkmans';
    case StijndeMilliano         = 'Stijn de Milliano';
    case BartvanAssem            = 'Bart van Assem';
    case GuyDubois               = 'Guy Dubois';
    case MartijnWernaert         = 'Martijn Wernaert';
    case MaykelvanVeggel         = 'Maykel van Veggel';
    case WillemvanBerlo          = 'Willem van Berlo';
    case FrankKemps              = 'Frank Kemps';
    case RikvanderLinde          = 'Rik van der Linde';
    case JoostvanMaasakkers      = 'Joost van Maasakkers';
    case KoenKukken              = 'Koen Kukken';
    case EricCuijpers            = 'Eric Cuijpers';
    case FrankDamen              = 'Frank Damen';
    case TatianaMorokko          = 'Tatiana Morokko';
    case DaanStrijbosch          = 'Daan Strijbosch';
    case NilsvanGriethuijsen     = 'Nils van Griethuijsen';
    case LiekeHuengesWajer       = 'Lieke Huenges Wajer';
    case NikkiHamers             = 'Nikki Hamers';
    case WendyBos                = 'Wendy Bos';
    case RandySchilling          = 'Randy Schilling';
    case JeroenThoolen           = 'Jeroen Thoolen';
    case RoelKnapen              = 'Roel Knapen';
    case ArthurTurksma           = 'Arthur Turksma';
    case RickCuijpers            = 'Rick Cuijpers';
    case LindyOverdiek           = 'Lindy Overdiek';
    case SteffieLoots            = 'Steffie Loots';

    public function getGuestType(): GuestType
    {
        return match ($this) {
            self::JeroenBerkvens,
            self::JennekeHeere,
            self::ChantalBerkvens => GuestType::Organizers,
            self::CarolineVanDenPol,
            self::TheoDuPont,
            self::NielsHeere,
            self::RickHeere,
            self::MarinusHeere,
            self::ThomBerkvens,
            self::HettyBerkvens,
            self::VictorSliphorst,
            self::EvaSliphorst,
            self::MarianneSliphorst,
            self::LucSliphorst,
            self::RuudStraten,
            self::AnoukBerkvens,
            self::DaanVanRooij,
            self::JoeyVanRooij,
            self::RosalieVanRooij,
            self::GemmaMeuwissen,
            self::LeonieBerkvens => GuestType::Close,
            self::AlainDuPont,
            self::MireilleVandenBarselaar,
            self::FrankVandenBarselaar,
            self::BjornVandenBarselaar,
            self::OlivierVandenBarselaar,
            self::CarmenVandenBarselaar,
            self::SaryaVanderKuij,
            self::AmberVanderKuij,
            self::EllenHeere,
            self::AlbertHogeweij,
            self::KeesVandenPol,
            self::EllisVandenPol,
            self::RemcoVandenPol,
            self::LeticiaAssuncoaCordeiro,
            self::MarkVandenPol,
            self::MarlieMeulenberg,
            self::NomiVandenPol,
            self::AkuaNielen,
            self::MaartenNielen,
            self::ReinierNielenKaal,
            self::AnnaNielenKaal,
            self::ErikNielen,
            self::JasmijnNielen,
            self::WouterNielen,
            self::JudithNowee,
            self::MarcelNowee,
            self::GwenNowee,
            self::MaxSnoeijen,
            self::MyrteNowee,
            self::MaxRoelofs,
            self::StijnNowee,
            self::DiedeVanVleuten,
            self::RonaldVanGenabeek,
            self::IngridVanGenabeek,
            self::JohannusBerkvens,
            self::CatherinaBerkvens,
            self::AnnabelTangel,
            self::JosephineBerkvens,
            self::MathildeBerkvens,
            self::BoukeBarnas,
            self::MariaBarnas,
            self::JohannesBerkvens,
            self::AnnikaBerkvensVisser,
            self::JustBerkvens,
            self::HiddeBerkvens,
            self::GwenBerkvens,
            self::AnnickBerkvens,
            self::SanderTeerling,
            self::CynthiaTeerling,
            self::ManoahTeerling,
            self::RoosTeerling,
            self::BartFluitsma,
            self::NasimaFluitsma,
            self::MaaikeLeahe,
            self::DominicLeahe,
            self::MadeliefLeahe,
            self::HamishLeahe,
            self::JoostFluitsma,
            self::ChristiaanTeerling,
            self::AnetteTeerling,
            self::MichielTeerling,
            self::HannaTangel,
            self::HielkeFluitsma,
            self::RennekeFluitsma,
            self::JosephineTeerling,
            self::JaimyBaartmans,
            self::MariskaMuijtjens,
            self::RemcoMuijtjens,
            self::ElenaMuijtjens,
            self::MarieAnneToonen,
            self::NickWanmaker,
            self::ThomasFranken,
            self::AstridBelder   => GuestType::Day,
            self::NicolienVanDongen,
            self::BramHurkmans,
            self::StijndeMilliano,
            self::BartvanAssem,
            self::GuyDubois,
            self::MartijnWernaert,
            self::MaykelvanVeggel,
            self::WillemvanBerlo,
            self::FrankKemps,
            self::RikvanderLinde,
            self::JoostvanMaasakkers,
            self::KoenKukken,
            self::EricCuijpers,
            self::FrankDamen,
            self::TatianaMorokko,
            self::DaanStrijbosch,
            self::NilsvanGriethuijsen,
            self::LiekeHuengesWajer,
            self::NikkiHamers,
            self::WendyBos,
            self::RandySchilling,
            self::JeroenThoolen,
            self::RoelKnapen,
            self::ArthurTurksma,
            self::RickCuijpers,
            self::LindyOverdiek,
            self::SteffieLoots   => GuestType::Party,
        };
    }

    /**
     * @return list<Person>
     */
    public function controls(): array
    {
        return match ($this) {
            self::JeroenBerkvens,
            self::JennekeHeere            => array_filter(self::cases(), fn (self $person) => $person !== self::JeroenBerkvens && $person !== self::JennekeHeere),
            self::CarolineVanDenPol       => [self::TheoDuPont],
            self::TheoDuPont              => [self::CarolineVanDenPol],
            self::ThomBerkvens            => [self::HettyBerkvens],
            self::HettyBerkvens           => [self::ThomBerkvens],
            self::MarianneSliphorst       => [self::LucSliphorst, self::VictorSliphorst, self::EvaSliphorst],
            self::LucSliphorst            => [self::MarianneSliphorst, self::VictorSliphorst, self::EvaSliphorst],
            self::ChantalBerkvens         => [self::RuudStraten],
            self::RuudStraten             => [self::ChantalBerkvens],
            self::AnoukBerkvens           => [self::DaanVanRooij, self::JoeyVanRooij, self::RosalieVanRooij],
            self::DaanVanRooij            => [self::AnoukBerkvens, self::JoeyVanRooij, self::RosalieVanRooij],
            self::MireilleVandenBarselaar => [self::MarinusHeere, self::FrankVandenBarselaar, self::BjornVandenBarselaar, self::OlivierVandenBarselaar, self::CarmenVandenBarselaar, self::EllenHeere, self::AlbertHogeweij, self::SaryaVanderKuij, self::AmberVanderKuij],
            self::FrankVandenBarselaar    => [self::MireilleVandenBarselaar, self::BjornVandenBarselaar, self::OlivierVandenBarselaar, self::CarmenVandenBarselaar],
            self::EllenHeere              => [self::MarinusHeere, self::AlbertHogeweij, self::SaryaVanderKuij, self::AmberVanderKuij],
            self::AlbertHogeweij          => [self::EllenHeere, self::SaryaVanderKuij, self::AmberVanderKuij],
            self::KeesVandenPol           => [self::EllisVandenPol, self::RemcoVandenPol, self::LeticiaAssuncoaCordeiro],
            self::EllisVandenPol          => [self::KeesVandenPol, self::RemcoVandenPol, self::LeticiaAssuncoaCordeiro],
            self::RemcoVandenPol          => [self::LeticiaAssuncoaCordeiro],
            self::LeticiaAssuncoaCordeiro => [self::RemcoVandenPol],
            self::MarkVandenPol           => [self::MarlieMeulenberg, self::NomiVandenPol],
            self::MarlieMeulenberg        => [self::MarkVandenPol, self::NomiVandenPol],
            self::AkuaNielen              => [self::MaartenNielen],
            self::MaartenNielen           => [self::AkuaNielen],
            self::ReinierNielenKaal       => [self::AnnaNielenKaal, self::ErikNielen, self::JasmijnNielen],
            self::AnnaNielenKaal          => [self::ReinierNielenKaal, self::ErikNielen, self::JasmijnNielen],
            self::JudithNowee             => [self::MarcelNowee],
            self::MarcelNowee             => [self::JudithNowee],
            self::GwenNowee               => [self::MaxSnoeijen],
            self::MaxSnoeijen             => [self::GwenNowee],
            self::MyrteNowee              => [self::MaxRoelofs],
            self::MaxRoelofs              => [self::MyrteNowee],
            self::StijnNowee              => [self::DiedeVanVleuten],
            self::DiedeVanVleuten         => [self::StijnNowee],
            self::RonaldVanGenabeek       => [self::IngridVanGenabeek],
            self::IngridVanGenabeek       => [self::RonaldVanGenabeek],
            self::JohannusBerkvens        => [self::CatherinaBerkvens, self::HannaTangel, self::AnnabelTangel],
            self::CatherinaBerkvens       => [self::JohannusBerkvens, self::HannaTangel, self::AnnabelTangel],
            self::JosephineBerkvens       => [self::MichielTeerling],
            self::MichielTeerling         => [self::JosephineBerkvens],
            self::BoukeBarnas             => [self::MariaBarnas],
            self::MariaBarnas             => [self::BoukeBarnas],
            self::JohannesBerkvens        => [self::AnnikaBerkvensVisser, self::JustBerkvens, self::HiddeBerkvens, self::GwenBerkvens, self::AnnickBerkvens],
            self::AnnikaBerkvensVisser    => [self::JohannesBerkvens, self::JustBerkvens, self::HiddeBerkvens, self::GwenBerkvens, self::AnnickBerkvens],
            self::SanderTeerling          => [self::CynthiaTeerling, self::ManoahTeerling, self::RoosTeerling],
            self::CynthiaTeerling         => [self::SanderTeerling, self::ManoahTeerling, self::RoosTeerling],
            self::BartFluitsma            => [self::NasimaFluitsma],
            self::NasimaFluitsma          => [self::BartFluitsma],
            self::MaaikeLeahe             => [self::DominicLeahe, self::MadeliefLeahe, self::HamishLeahe],
            self::DominicLeahe            => [self::MaaikeLeahe, self::MadeliefLeahe, self::HamishLeahe],
            self::ChristiaanTeerling      => [self::AnetteTeerling],
            self::AnetteTeerling          => [self::ChristiaanTeerling],
            self::HielkeFluitsma          => [self::RennekeFluitsma],
            self::RennekeFluitsma         => [self::HielkeFluitsma],
            self::MariskaMuijtjens        => [self::RemcoMuijtjens, self::ElenaMuijtjens],
            self::RemcoMuijtjens          => [self::MariskaMuijtjens, self::ElenaMuijtjens],
            self::MarieAnneToonen         => [self::NickWanmaker],
            self::NickWanmaker            => [self::MarieAnneToonen],
            self::NielsHeere,
            self::RickHeere,
            self::MarinusHeere,
            self::AlainDuPont,
            self::VictorSliphorst,
            self::EvaSliphorst,
            self::JoeyVanRooij,
            self::RosalieVanRooij,
            self::BjornVandenBarselaar,
            self::OlivierVandenBarselaar,
            self::CarmenVandenBarselaar,
            self::SaryaVanderKuij,
            self::AmberVanderKuij,
            self::NomiVandenPol,
            self::GemmaMeuwissen,
            self::ErikNielen,
            self::JasmijnNielen,
            self::WouterNielen,
            self::LeonieBerkvens,
            self::AnnabelTangel,
            self::MathildeBerkvens,
            self::JustBerkvens,
            self::HiddeBerkvens,
            self::GwenBerkvens,
            self::AnnickBerkvens,
            self::ManoahTeerling,
            self::RoosTeerling,
            self::MadeliefLeahe,
            self::HamishLeahe,
            self::JoostFluitsma,
            self::HannaTangel,
            self::JosephineTeerling,
            self::JaimyBaartmans,
            self::ElenaMuijtjens,
            self::ThomasFranken,
            self::AstridBelder,
            self::NicolienVanDongen,
            self::BramHurkmans,
            self::StijndeMilliano,
            self::BartvanAssem,
            self::GuyDubois,
            self::MartijnWernaert,
            self::MaykelvanVeggel,
            self::WillemvanBerlo,
            self::FrankKemps,
            self::RikvanderLinde,
            self::JoostvanMaasakkers,
            self::KoenKukken,
            self::EricCuijpers,
            self::FrankDamen,
            self::TatianaMorokko,
            self::DaanStrijbosch,
            self::NilsvanGriethuijsen,
            self::LiekeHuengesWajer,
            self::NikkiHamers,
            self::WendyBos,
            self::RandySchilling,
            self::JeroenThoolen,
            self::RoelKnapen,
            self::ArthurTurksma,
            self::RickCuijpers,
            self::LindyOverdiek,
            self::SteffieLoots            => [],
        };
    }

    public function hasPlusOne(): bool
    {
        return match ($this) {
            self::NielsHeere,
            self::AlainDuPont,
            self::BjornVandenBarselaar,
            self::OlivierVandenBarselaar,
            self::CarmenVandenBarselaar,
            self::SaryaVanderKuij,
            self::AmberVanderKuij,
            self::WouterNielen,
            self::JoostFluitsma,
            self::MichielTeerling,
            self::HannaTangel,
            self::JosephineTeerling,
            self::JaimyBaartmans,
            self::ThomasFranken,
            self::AstridBelder,
            self::NicolienVanDongen,
            self::BramHurkmans,
            self::StijndeMilliano,
            self::BartvanAssem,
            self::GuyDubois,
            self::RickHeere,
            self::LindyOverdiek,
            self::SteffieLoots => true,
            self::JeroenBerkvens,
            self::JennekeHeere,
            self::CarolineVanDenPol,
            self::TheoDuPont,
            self::MarinusHeere,
            self::ThomBerkvens,
            self::HettyBerkvens,
            self::VictorSliphorst,
            self::EvaSliphorst,
            self::MarianneSliphorst,
            self::LucSliphorst,
            self::ChantalBerkvens,
            self::RuudStraten,
            self::AnoukBerkvens,
            self::DaanVanRooij,
            self::JoeyVanRooij,
            self::RosalieVanRooij,
            self::MireilleVandenBarselaar,
            self::FrankVandenBarselaar,
            self::EllenHeere,
            self::AlbertHogeweij,
            self::KeesVandenPol,
            self::EllisVandenPol,
            self::RemcoVandenPol,
            self::LeticiaAssuncoaCordeiro,
            self::MarkVandenPol,
            self::MarlieMeulenberg,
            self::NomiVandenPol,
            self::GemmaMeuwissen,
            self::AkuaNielen,
            self::MaartenNielen,
            self::ReinierNielenKaal,
            self::AnnaNielenKaal,
            self::ErikNielen,
            self::JasmijnNielen,
            self::JudithNowee,
            self::MarcelNowee,
            self::GwenNowee,
            self::MaxSnoeijen,
            self::MyrteNowee,
            self::MaxRoelofs,
            self::StijnNowee,
            self::DiedeVanVleuten,
            self::RonaldVanGenabeek,
            self::IngridVanGenabeek,
            self::LeonieBerkvens,
            self::JohannusBerkvens,
            self::CatherinaBerkvens,
            self::AnnabelTangel,
            self::JosephineBerkvens,
            self::MathildeBerkvens,
            self::BoukeBarnas,
            self::MariaBarnas,
            self::JohannesBerkvens,
            self::AnnikaBerkvensVisser,
            self::JustBerkvens,
            self::HiddeBerkvens,
            self::GwenBerkvens,
            self::AnnickBerkvens,
            self::SanderTeerling,
            self::CynthiaTeerling,
            self::ManoahTeerling,
            self::RoosTeerling,
            self::BartFluitsma,
            self::NasimaFluitsma,
            self::MaaikeLeahe,
            self::DominicLeahe,
            self::MadeliefLeahe,
            self::HamishLeahe,
            self::ChristiaanTeerling,
            self::AnetteTeerling,
            self::HielkeFluitsma,
            self::RennekeFluitsma,
            self::MariskaMuijtjens,
            self::RemcoMuijtjens,
            self::ElenaMuijtjens,
            self::MarieAnneToonen,
            self::NickWanmaker,
            self::MartijnWernaert,
            self::MaykelvanVeggel,
            self::WillemvanBerlo,
            self::FrankKemps,
            self::RikvanderLinde,
            self::JoostvanMaasakkers,
            self::KoenKukken,
            self::EricCuijpers,
            self::FrankDamen,
            self::TatianaMorokko,
            self::DaanStrijbosch,
            self::NilsvanGriethuijsen,
            self::LiekeHuengesWajer,
            self::NikkiHamers,
            self::WendyBos,
            self::RandySchilling,
            self::JeroenThoolen,
            self::RoelKnapen,
            self::ArthurTurksma,
            self::RickCuijpers => false,
        };
    }

    public function rsvp(): Rsvp
    {
        return $this->getRsvp(false);
    }

    public function plusOneRsvp(): Rsvp
    {
        return $this->getRsvp(true);
    }

    private function getRsvp(bool $plusOne): Rsvp
    {
        static $cache = [];
        if (!array_key_exists($this->value, $cache)) {
            $cache[$this->value] = [];
        }
        if (array_key_exists((int) $plusOne, $cache[$this->value])) {
            return $cache[$this->value][(int) $plusOne];
        }
        $found = Rsvp::wherePerson($this->value)->where('plusOne', '=', $plusOne)->first();
        if ($found?->exists) {
            return $found;
        }
        $rsvpData = [
            'person'   => $this,
            'name'     => $plusOne ? '' : $this->value,
            'dietType' => DietType::Normal,
            'plusOne'  => $plusOne,
        ];
        $rsvpData += $this->getGuestType()->getDefaultRsvpTimeSlotData();
        if (!$plusOne) {
            if ($this === Person::ThomasFranken) {
                $rsvpData[TimeSlot::Breakfast->value] = true;
            }
        }
        $cache[$this->value][(int) $plusOne] = Rsvp::firstOrNew($rsvpData);
        return $cache[$this->value][(int) $plusOne];
    }

    public function otherRsvpsFilled(): bool
    {
        static $cache = [];
        if (array_key_exists($this->value, $cache)) {
            return $cache[$this->value];
        }
        foreach ($this->controls() as $controlledPerson) {
            if (!Rsvp::wherePerson($controlledPerson->value)->where('plusOne', '=', false)->exists()) {
                $cache[$this->value] = false;
                return false;
            }
        }
        if ($this->hasPlusOne()) {
            $cache[$this->value] = Rsvp::wherePerson($this->value)->where('plusOne', '=', true)->exists();
            return $cache[$this->value];
        }
        $cache[$this->value] = true;
        return true;
    }

    public function getLocale(): Locale
    {
        return match ($this) {
            self::DominicLeahe,
            self::HamishLeahe,
            self::MadeliefLeahe => Locale::English,
            default             => Locale::Dutch
        };
    }

    /**
     * @return list<self>
     */
    public static function sorted(PersonSort $personSort): array
    {
        $persons = self::cases();
        usort($persons, $personSort->sort(...));
        return $persons;
    }

    /**
     * @return list<self>
     */
    public static function missing(bool $all = false, bool $includeController = true, bool $includeImplied = false): array
    {
        $cacheKey = $includeController ? 0 : 1;
        $cacheKey += $includeImplied ? 2 : 4;
        static $cache = null;
        if ($cache !== null) {
            if (array_key_exists($cacheKey, $cache)) {
                return $cache[$cacheKey];
            }
        } else {
            $cache = [];
        }
        $persons = [];
        foreach (self::cases() as $person) {
            if (
                $person->getGuestType() === GuestType::Organizers
                || $person->comingState() === ComingState::Coming
                || $person->comingState() === ComingState::NotComing
                || (!$includeImplied && $person->comingState() === ComingState::ImpliedNotComing)
                || (!$all && !$person->showInMissing())
            ) {
                continue;
            }
            if ($includeController && $person->getControllerWithPhoneNumber() !== null) {
                $persons[$person->getControllerWithPhoneNumber()->value] = $person->getControllerWithPhoneNumber();
            }
            $persons[$person->value] = $person;
        }
        $cache[$cacheKey] = array_values($persons);
        return $cache[$cacheKey];
    }

    public function getName(): string
    {
        return $this->rsvp()->name ?? $this->value;
    }

    public function hasAccount(): bool
    {
        static $cache = [];
        if (array_key_exists($this->value, $cache)) {
            return $cache[$this->value];
        }
        $cache[$this->value] = User::whereName($this->value)?->exists();
        return $cache[$this->value];
    }

    private function showInMissing(): bool
    {
        return !$this->doNotShowAsMissing() && !$this->rsvp()->exists;
    }

    private function doNotShowAsMissing(): bool
    {
        return match ($this) {
            self::JeroenBerkvens,
            self::JennekeHeere,
            self::ChantalBerkvens,
            self::RickHeere,
            self::MartijnWernaert,
            self::MaykelvanVeggel,
            self::WillemvanBerlo,
            self::FrankKemps,
            self::RikvanderLinde,
            self::JoostvanMaasakkers,
            self::KoenKukken,
            self::EricCuijpers,
            self::FrankDamen,
            self::TatianaMorokko,
            self::DaanStrijbosch,
            self::NilsvanGriethuijsen,
            self::LiekeHuengesWajer,
            self::NikkiHamers,
            self::WendyBos,
            self::RandySchilling,
            self::JeroenThoolen,
            self::RoelKnapen,
            self::ArthurTurksma,
            self::RickCuijpers,
            self::NicolienVanDongen => true,
            default                 => false,
        };
    }

    public function getPhoneNumber(): string
    {
        return env('PHONE_'.strtoupper($this->name), '');
    }

    public function getControllerWithPhoneNumber(): ?self
    {
        foreach (self::controllers() as $person) {
            if ($person->getGuestType() !== GuestType::Organizers && $person->getPhoneNumber() !== '') {
                return $person;
            }
        }
        return null;
    }

    /**
     * @return list<self>
     */
    public function controllers(): array
    {
        static $cache = null;
        if ($cache !== null) {
            if (array_key_exists($this->name, $cache)) {
                return $cache[$this->name];
            }
        } else {
            $cache = [];
        }
        $cache[$this->name] = [];
        foreach (self::cases() as $person) {
            if (in_array($this, $person->controls(), true)) {
                $cache[$this->name][] = $person;
            }
        }
        return $cache[$this->name];
    }

    public static function count(array $persons = null, bool $includePlusOne = true): int
    {
        if ($persons === null) {
            $persons = self::cases();
        }

        $count = 0;
        foreach ($persons as $person) {
            if ($person->getGuestType() === GuestType::Organizers) {
                continue;
            }
            $count++;
            if ($includePlusOne && $person->hasPlusOne()) {
                $count++;
            }
        }
        return $count;
    }

    public static function countByType(PersonCountType $type, bool $includePlusOne = false): int
    {
        static $cache = null;
        $key = $includePlusOne ? 1 : 0;
        if ($cache !== null) {
            if (array_key_exists($key, $cache)) {
                return $cache[$key][$type->name];
            }
        } else {
            $cache = [];
        }
        $cache[$key] = array_fill_keys(PersonCountType::options(), 0);
        foreach (self::cases() as $person) {
            if ($person->getGuestType() === GuestType::Organizers) {
                continue;
            }
            $cache[$key][PersonCountType::All->name]++;
            switch ($person->comingState()) {
                case ComingState::Coming:
                    $cache[$key][PersonCountType::Reacted->name]++;
                    $cache[$key][PersonCountType::Coming->name]++;
                    break;
                case ComingState::NotComing:
                    $cache[$key][PersonCountType::Reacted->name]++;
                    $cache[$key][PersonCountType::NotComing->name]++;
                    break;
                case ComingState::ImpliedNotComing:
                    $cache[$key][PersonCountType::NoReaction->name]++;
                    $cache[$key][PersonCountType::ImpliedNotComing->name]++;
                    break;
                case ComingState::NoReaction:
                    $cache[$key][PersonCountType::NoReaction->name]++;
                    $cache[$key][PersonCountType::NoImpliedReaction->name]++;
                    break;
            }
            if ($includePlusOne) {
                $cache[$key][PersonCountType::All->name]++;
            }
        }
        return $cache[$key][$type->name];
    }

    public function comingState(): ComingState
    {
        if ($this->rsvp()->isComing()) {
            return ComingState::Coming;
        }
        if ($this->rsvp()->exists) {
            return ComingState::NotComing;
        }
        $controllerNotComing = false;
        foreach ($this->controllers() as $controller) {
            if (!$controller->rsvp()->exists) {
                continue;
            }
            if ($controller->rsvp()->isComing()) {
                return ComingState::NoReaction;
            }
            if (!$controller->rsvp()->isComing()) {
                $controllerNotComing = true;
            }
        }
        if ($controllerNotComing || !$this->showInMissing()) {
            return ComingState::ImpliedNotComing;
        }
        return ComingState::NoReaction;
    }
}
