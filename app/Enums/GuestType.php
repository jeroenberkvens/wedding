<?php
declare(strict_types=1);

namespace App\Enums;

use Illuminate\Validation\Rule;

enum GuestType
{
    case Organizers;
    case Close;
    case Day;
    case Party;

    /**
     * @return array<TimeSlot>
     */
    public function getTimeSlots(): array
    {
        return match ($this) {
            self::Organizers => TimeSlot::cases(),
            self::Close => TimeSlot::without(TimeSlot::DinerBistro),
            self::Day   => TimeSlot::without(TimeSlot::DinerClose),
            self::Party => [TimeSlot::Party],
        };
    }

    public function getRules(): array
    {
//        $allowed = ['boolean'];
//        $notAllowed = ['boolean'];
//        $dayBooleanRules = match ($this) {
//            self::Close,
//            self::Day   => $allowed,
//            self::Party => $notAllowed,
//        };
        return [
            'person'      => ['required'],
            'name'        => ['required', 'string', 'max:255', 'regex:/[a-zA-Z0-9\s]+/'],
            'allergens'   => ['nullable', 'string'],
            'dietType'    => ['required', 'string', Rule::in(DietType::caseValues())],
            'diet'        => ['nullable', 'string'],
            'lunch'       => ['boolean'],
            'ceremony'    => ['boolean'],
            'speech'      => ['boolean'],
            'reception'   => ['boolean'],
            'dinerClose'  => ['boolean'],
            'dinerBistro' => ['boolean'],
            'party'       => ['boolean'],
            'breakfast'   => ['boolean'],
        ];
    }

    public function getDefaultRsvpTimeSlotData(): array
    {
        $data = [];
        foreach (TimeSlot::cases() as $timeSlot) {
            $data[$timeSlot->value] = false;
        }
        foreach ($this->getTimeSlots() as $timeSlot) {
            $data[$timeSlot->value] = true;
        }
        if ($this === self::Day) {
            $data[TimeSlot::Breakfast->value] = false;
        }
        return $data;
    }
}
