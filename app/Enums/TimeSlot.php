<?php
declare(strict_types=1);

namespace App\Enums;

use BeTo\Laravel\Enums\GetCaseValuesTrait;

enum TimeSlot: string
{
    use GetCaseValuesTrait;

    case Lunch       = 'lunch';
    case Ceremony    = 'ceremony';
    case Speech      = 'speech';
    case Reception   = 'reception';
    case DinerClose  = 'dinerClose';
    case DinerBistro = 'dinerBistro';
    case Party       = 'party';
    case Breakfast   = 'breakfast';

    public static function without(TimeSlot ...$excluded): array
    {
        $timeSlots = [];
        foreach (self::cases() as $timeSlot) {
            if (!in_array($timeSlot, $excluded)) {
                $timeSlots[] = $timeSlot;
            }
        }
        return $timeSlots;
    }

    public function hasDescription(): bool
    {
        return match ($this) {
            self::Speech,
            self::Party     => false,
            self::Lunch,
            self::Ceremony,
            self::Reception,
            self::DinerClose,
            self::DinerBistro,
            self::Breakfast => true,
        };
    }

    public function getDay(): string
    {
        return $this !== self::Breakfast ? 'Zaterdag' : 'Zondag';
    }

    public function getTime(): string
    {
        if ($this === self::Breakfast) {
            return $this->getDay() . ' ' . $this->getTimeOnly();
        } else {
            return $this->getTimeOnly();
        }
    }

    public function getTimeOnly(): string
    {
        ob_start();
        $this->printStartTime();
        echo ' - ';
        $this->printEndTime();
        return ob_get_clean();
    }

    public function printStartTime(): void
    {
        echo match ($this) {
            self::Lunch       => '12:15',
            self::Ceremony    => '13:00',
            self::Speech      => '14:00',
            self::Reception   => '14:15',
            self::DinerBistro => '17:00',
            self::DinerClose  => '17:30',
            self::Party       => '20:00',
            self::Breakfast   => '09:00',
        };
    }

    public function printEndTime(): void
    {
        echo match ($this) {
            self::Lunch       => '13:00',
            self::Ceremony    => '14:00',
            self::Speech      => '14:15',
            self::Reception,
            self::DinerClose,
            self::DinerBistro => '20:00',
            self::Party       => '00:30',
            self::Breakfast   => '10:00',
        };
    }
}
