<?php
declare(strict_types=1);

namespace App\Enums;

enum ComingState
{
    case Coming;
    case NotComing;
    case ImpliedNotComing;
    case NoReaction;

    public function getHtml(): string
    {
        return match ($this) {
            self::Coming           => '<span class="text-green-500">✓</span>',
            self::NotComing        => '<span class="text-red-500">✗</span>',
            self::ImpliedNotComing => '<span class="text-yellow-500">✗</span>',
            self::NoReaction       => '<span class="text-yellow-500">?</span>',
        };
    }
}
