<?php
declare(strict_types=1);

namespace App\Enums;

enum PersonCountType
{
    case All;
    case Reacted;
    case Coming;
    case NotComing;
    case NoReaction;
    case NoImpliedReaction;
    case ImpliedNotComing;

    public static function options(): array
    {
        $options = [];
        foreach (self::cases() as $case) {
            $options[] = $case->name;
        }
        return $options;
    }
}
