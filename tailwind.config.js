const beto = require('./vendor/beto/laravel/tailwind');

module.exports = {
    ...beto,
    ...{
        content: [
            './app/Enums/ComingState.php',
            './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
            './vendor/laravel/jetstream/**/*.blade.php',
            './storage/framework/views/*.php',
            './resources/views/**/*.blade.php',
            './resources/js/**/*.js',
            './vendor/beto/**.js',
            './vendor/beto/**.blade.php',
            './vendor/masmerise/livewire-toaster/resources/views/*.blade.php'
        ],
        theme: {
            minHeight: {
                '10': '2.5rem',
            }
        }
    }
};
