<?php

return [
    'home'          => 'navigation.home',
    'dress-code'    => 'navigation.dress-code',
    'rsvp'          => 'navigation.rsvp',
    'planning'      => 'navigation.planning',
    'gift-registry' => 'navigation.gift-registry',
];
