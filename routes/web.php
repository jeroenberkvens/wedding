<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\RsvpController;
use App\Mail\RsvpUpdated;
use App\Models\User;
use BeTo\Laravel\Helpers\Routes;
use Illuminate\Support\Facades\Route;
use Laravel\Fortify\Http\Controllers\AuthenticatedSessionController;
use Laravel\Fortify\RoutePath;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::post('/login', [LoginController::class, 'login'])->name('passwordless-login');

Routes::get('home', '/', 'welcome');
Routes::get('dress-code');
Route::get('/rsvp', [RsvpController::class, 'rsvp'])->name('rsvp');
Route::get('/rsvp/{person}', [RsvpController::class, 'rsvpForPerson'])->name('rsvp.control');
Route::get('/rsvp/{person}/plus-one', [RsvpController::class, 'rsvpForPerson'])->name('rsvp.plusOne');
Routes::get('planning');
Routes::get('gift-registry');
Route::middleware('organizer')->group(function () {
    Routes::get('overview');
    Routes::get('missing');
    Routes::get('missing-only');
    Routes::get('guest-list');
    Routes::get('gift-registry-overview');
});

Route::put('/set-warning-shown', function () {
    User::tryCurrent()?->update(['warn' => false]);
    session()->put('warn', false);
    return response()->noContent();
})->name('set-warning-shown');

Route::get(RoutePath::for('logout', '/logout'), [AuthenticatedSessionController::class, 'destroy'])->name('logout'); // TODO @JB remove

Route::get('/mailable/{to}/{actor}', function (User $to, User $actor) {
    return new RsvpUpdated($actor, $to);
});
