<?php

return [
    'missing'            => 'No Reaction',
    'reacted'            => 'Reacted',
    'coming'             => 'Coming',
    'implied-not-coming' => 'Controller isn\'t coming',
];
