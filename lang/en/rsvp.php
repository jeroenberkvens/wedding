<?php

return [
    'notice'            => 'We ask everyone to keep your pets at home. Kids are welcome, but please keep them quiet during the ceremony and excuse yourself if needed.',
    'name'              => 'Name',
    'planning'          => 'Planning',
    'allergens'         => 'Allergens',
    'allergens-note'    => 'Only specify things you\'re actually allergic to',
    'diet-label'        => 'Dietary wishes',
    'diet-types'        => [
        'normal'     => 'Normal',
        'vegetarian' => 'Vegetarian',
        'vegan'      => 'Vegan',
    ],
    'diet'              => 'Other dietary wishes',
    'diet-note'         => 'Here you can enter other preferences',
    'none'              => '--- NONE ---',
    'save'              => 'Save',
    'updated'           => 'Changes Saved',
    'save-warning'      => [
        'title'       => 'Warning',
        'explanation' => 'The RSVP has not been saved yet. Use the "Save" button below the form.',
    ],
    'email'             => [
        'header'    => 'RSVP updated',
        'body'      => ':actor has updated your RSVP. Check if everything is correct.',
        'field'     => 'Field',
        'value'     => 'Value',
        'button'    => 'Update',
        'greetings' => 'Greetings',
    ],
    'total'             => 'Total guests',
    'party-only'        => 'Party guests',
    'coming'            => 'Coming',
    'not-coming'        => 'Cancellations',
    'user-without-rsvp' => 'Users without RSVP',
    'expected'          => 'Expected',
    'plus-one'          => 'Plus One',
];
