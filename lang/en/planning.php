<?php

return [
    'labels'       => [
        'lunch'       => 'Welcome with Lunch',
        'ceremony'    => 'Ceremony',
        'speech'      => 'Speech and Cake',
        'reception'   => 'Reception',
        'dinerClose'  => 'Diner in the "Druivenkas"',
        'dinerBistro' => 'Bistro Diner',
        'party'       => 'Party',
        'breakfast'   => 'Breakfast',
    ],
    'descriptions' => [
        'lunch'       => [
            'When arriving at "Landgoed Groot Warnsborn", a large sign will point you to the parking space next to the oranjerie.',
            'Starting at 12:15, there will be welcome in the "Orchideeënkas". It won\'t be necessary to lunch before as coffee, tea and deluxe sandwiches will be provided. There will also be something for the kids.',
        ],
        'ceremony'    => [
            'The wedding ceremony will take place between 13:00 and 14:00. If the weather allows it, it will be in the gardens. If not, we\'ll have it inside the "oranjerie" instead.',
        ],
        'reception'   => [
            'After the Speech and Cake there will be time for congratulations and drinks. There will also be time for speeches, presentations and such (a beamer is available). If you want to do something, please contact the wedding planner.',
            'Starting at 16:00 we\'re going to make some group pictures. Afterwards the newlyweds are going to take some more pictures.',
        ],
        'dinerClose'  => [
            'Starting at 17:30 a select group including you and the newlyweds have a three course dinner in the "druivenkas".',
        ],
        'dinerBistro' => [
            'Starting at 17:00 the dinner will be available for the guests in the hotel.',
        ],
        'breakfast'   => [
            'We would like to share a breakfast with everyone that stays the night at the hotel or somewhere nearby. For those staying in the hotel, the breakfast is included. The breakfast will be at the hotel between 9:00 and 9:30',
        ],
    ],
];
