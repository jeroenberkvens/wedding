<?php

return [
    'header'      => 'Dress Code',
    'title'       => 'Black Tie Optional',
    'description' => 'Because we try our best to make this a beautiful wedding, we hope everyone will help by reading it carefully and try to adhere to our dress code as much as possible. If you have a question if something does or does not fit within the guidelines or you have other questions about it, send an email to :email.',
    'men'      => [
        'title'       => 'For the Men',
        'description' => 'A black tie optional wedding is basically suggesting that you should wear black tie, but it\'s acceptable if you don\'t. So what does that mean, exactly? Men can wear dark suits (think: your very best suit, preferably black and custom-tailored) instead of the tux that\'s required for actual black tie weddings. Women can wear very fancy cocktail dresses instead of floor-length dresses, but avoid anything with revealing cutouts or hemlines above the knee.',
    ],
    'renting'  => [
        'title'       => 'Renting a Tux',
        'description' => 'For everyone that doesn\'t have a suite, it is also possible to rent a Tux. I\'m getting my suite from :blackTie in Den Bosch and they also rent out Tuxedos', // TODO @JB check tux vs tuxedo vs smoking
    ],
    'women'    => [
        'title'       => 'For the Women',
        'description' => 'For women, the Black Tie dress code specifies a floor-length dress, but with Black Tie Optional you can also wear a very fancy cocktail dress instead, but avoid anything with revealing cutouts or hemlines above the knee.',
    ],
    'shorter'  => [
        'title'       => 'Length & Color',
        'description' => 'We ask to choose a dress with the hemline below the knee, and for color: :important as those are the colors of the wedding dress',
        'important'   => 'no red nor white',
    ],
    'examples' => [
        'black-tie'      => 'Black-tie',
        'summer-wedding' => 'Summer Wedding',
        'too-short'      => 'Too Short',
    ],
    'warning'  => [
        'title'       => 'Important!',
        'description' => 'Do not wear a red or white dress. These colors are reserved for the bride.',
    ],
];
