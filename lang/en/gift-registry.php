<?php

return [
    'intro'   => [
        'header'       => 'Important Information',
        'descriptions' => [
            'We would like to create a complete tableware set from our wedding presents. Below you can find all the parts that we\'ve chosen for our tableware set with a link to the webshop.',
            'Because we want to create a complete set it is important to us that all items are from the same set.',
            'Tip: First select the item and how many you\'re buying before you actually buy it to prevent other guests from buying the same thing.',
            //            'An envelope would also be much appreciated.',
        ],
    ],
    'headers' => [
        'item'    => 'Item',
        'bought'  => 'Bought',
        'total'   => 'Total',
        'actions' => 'Actions',
    ],
    'fields' => [
        'amount' => 'Amount',
    ],
    'actions' => [
        'cancel' => 'Never mind',
        'buy'    => 'I want to give this',
        'bought' => 'I\'ve bought :count',
        'edit'   => 'Edit',
    ],
    'notifications' => [
        'thanks' => 'Thank you for wanting to give this, we\'ve updated the list.',
        'never-mind' => 'We\'ve updated the list.',
    ],
    'items'   => [
        'eetbord'                    => 'Dinner plate',
        'universeel-bord'            => 'Universal Bowl',
        'ontbijtbord'                => 'Breakfast plate',
        'broodbord'                  => 'Bread plate',
        'gourmetbord'                => 'Gourmet plate',
        'rechthoekige-serveerschaal' => 'Serving plate (large)',
        'multifunctioneel-bord'      => 'Serving plate (small)',
        'pastabord'                  => 'Pasta plate',
        'dipschaaltje'               => 'Bowl (small)',
        'witte-wijnglas'             => 'Wineglass (white)',
        'rode-wijnglas'              => 'Wineglass (red)',
        'champagneglas'              => 'Champagne glass',
        'whiskyglas'                 => 'Universal glass',
        'platte-schaal'              => 'Deep plate',
        'bol'                        => 'Soup bowl',
        'koffieschotel'              => 'Coffee saucer',
        'espresso-schotel'           => 'Espresso saucer',
        'cafe-au-lait-schotel'       => 'Tea saucer',
        'koffiekop'                  => 'Coffee cup',
        'beker-l'                    => 'Cup (large)',
        'espressokopje'              => 'Espresso cup',
        'beker-s'                    => 'Cup (small)',
    ],
];
