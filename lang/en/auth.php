<?php

return [
    'name'               => 'Name',
    'email'              => 'Email',
    'already-registered' => 'Already Registered?',
    'register'           => 'Register',
    'login'              => 'Login',
    'logout'             => 'Logout',
    'account'            => 'Account',
];
