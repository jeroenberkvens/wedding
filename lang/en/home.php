<?php

return [
    'header'     => 'Wedding Jeroen & Jenneke',
    'welcome'    => [
        'header' => 'Welcome',
        'title'  => 'Welcome on our website',
        'body'   => [
            'We\'re using this website to provide our guests with extra information',
        ],
    ],
    'general'    => [
        'title' => 'General',
        'body'  => 'It isn\'t necessary to make photos as we have photographers for that.',
    ],
    'location'   => [
        'title'      => 'Location',
        'body'       => 'The entire wedding and the party all take place at Landgoed Groot Warnsborn:',
        'directions' => 'Route',
    ],
    'rsvp'       => [
        'header' => 'RSVP',
        'text'   => 'Here you can enter what parts you and your partner / plus 1 / children will and will not be attending, and other important information such as dietary wishes.',
    ],
    'dress-code' => [
        'header' => 'Dress Code',
        'text'   => 'Our Dress Code is Black Tie Optional. Get more information about what this means and other important information about what is and isn\'t proper.',
    ],
];
