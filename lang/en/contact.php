<?php

return [
    'chantal' => [
        'name'        => 'Chantal Berkvens',
        'description' => 'Chantal helps us with organizing lots of things. If you have any questions, send an email to :email',
    ],
    'phone-number' => 'Phone Number',
];
