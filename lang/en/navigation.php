<?php

return [
    'home'                   => 'Home',
    'dress-code'             => 'Dress Code',
    'rsvp'                   => 'RSVP',
    'planning'               => 'Planning',
    'gift-registry'          => 'Gift Registry',
    'overview'               => 'Overview',
    'gift-registry-overview' => 'Gift Registry',
    'read-more'              => 'Read More',
    'language-toggle'        => 'NL',
    'guest-list'             => 'Guestlist',
    'missing'                => 'Missing',
];
