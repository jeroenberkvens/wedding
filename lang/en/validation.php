<?php

return [
    'something' => 'Oops! Something went wrong.',
    'name'      => [
        'required' => 'You need to enter your name.',
        'string'   => 'Your name is only allowed to have letters and spaces.',
        'regex'    => 'Your name is only allowed to have letters and spaces.',
        'max'      => 'Your name is too long.',
        'in'       => 'We couldn\'t find this name in our guest list. If your name is misspelled of if you want it written differently, you can change it in the RSVP.',
    ],
    'email'     => [
        'required'  => 'The email address is required',
        'something' => 'The email address seems to be incorrect.',
        'max'       => 'The email address is too long.',
        'exists'    => 'The email address is not known in our system.',
        'unique'    => 'This email address is already in use by someone else.',
    ],
    'amount'    => [
        'required' => 'You need to enter the amount you\'re going to buy.',
        'int'      => 'The number you\'ve entered is not an integer.',
        'min'      => 'The amount cannot be less than 1. If you don\'t want to buy this, use that button.',
        'max'      => 'It seems you want to buy more than are still needed.',
    ],
    'allergens' => 'Something went wrong with entering your allergens.',
    'dietType'  => [
        'required' => 'Choose one of the dietary wishes.',
        'string'   => 'Something went wrong with entering your dietary wishes.',
    ],
    'diet'      => 'Something went wrong with entering your other dietary wishes.',
    'planning'  => 'Something went wrong with entering your planning.',
];
