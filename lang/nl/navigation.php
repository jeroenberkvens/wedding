<?php

return [
    'home'                   => 'Home',
    'dress-code'             => 'Kledingvoorschrift',
    'rsvp'                   => 'RSVP',
    'planning'               => 'Planning',
    'gift-registry'          => 'Cadeaulijst',
    'overview'               => 'Overzicht',
    'gift-registry-overview' => 'Cadeaulijst',
    'read-more'              => 'Lees meer',
    'language-toggle'        => 'EN',
    'guest-list'             => 'Gastenlijst',
    'missing'                => 'Geen Reactie',
];
