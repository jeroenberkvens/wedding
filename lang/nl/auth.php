<?php

return [
    'name'               => 'Naam',
    'email'              => 'Email',
    'already-registered' => 'Heb je al geregistreerd?',
    'register'           => 'Registreer',
    'login'              => 'Login',
    'logout'             => 'Uitloggen',
    'account'            => 'Account',
];
