<?php

return [
    'labels'       => [
        'lunch'       => 'Ontvangst',
        'ceremony'    => 'Ceremonie',
        'speech'      => 'Toost & Taart',
        'reception'   => 'Receptie',
        'dinerClose'  => 'Diner in de Druivenkas',
        'dinerBistro' => 'Bistro Diner',
        'party'       => 'Feest',
        'breakfast'   => 'Ontbijt',
    ],
    'descriptions' => [
        'lunch'       => [
            'Als u aan komt rijden bij Landgoed Groot Warnsborn zal een groot bord u naar de juiste parkeerplaats naast de oranjerie leiden.',
            'Vanaf 12:15 vindt de ontvangst plaats in de Orchideeënkas. Vooraf lunchen is niet nodig, want koffie, thee en luxe belegde broodjes zullen aanwezig zijn. Aan de kinderen wordt ook gedacht.',
        ],
        'ceremony'    => [
            'De trouwceremonie vindt plaats tussen 13:00 en 14:00. Bij mooi weer is dit in de tuinen van de oranjerie. Bij slecht weer vindt de ceremonie plaats in de oranjerie zelf.',
        ],
        'reception'   => [
            'Na de toost en taart is tot het diner tijd voor felicitaties en de borrel. Nu is er ook de mogelijkheid tot stukjes, speeches en presentaties, eventueel met een beamer. Als je een stukje wil doen, neem dan alsjeblieft contact op met de ceremoniemeester.',
            'Vanaf 16:00 willen we graag groepsfoto\'s maken. Hierna gaat het bruidspaar met de fotograaf foto\'s maken. De gasten kunnen tot het diner verder borrelen.',
        ],
        'dinerClose'  => [
            'Vanaf 17:30 vindt met een selecte groep waaronder jij/jullie en het bruidspaar een driegangendiner plaats in de druivenkas.',
        ],
        'dinerBistro' => [
            'Vanaf 17:00 vindt het diner voor de gasten plaats in het hotel. Het bruidspaar eet het diner met een selecte groep in de druivenkas.', // TODO @JB I thought you didn't want to specifically exclude people...
        ],
        'breakfast'   => [
            'We willen graag gezamenlijk ontbijten met degenen die bij het hotel of in de buurt blijven overnachten. Bij de hotelkamers zit dit inbegrepen. Het onbijt vind in het hotel plaats tussen 9:00 en 9:30.',
        ],
    ],
];
