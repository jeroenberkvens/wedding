<?php

return [
    'chantal'      => [
        'name'        => 'Chantal Berkvens',
        'description' => 'Chantal helpt ons met het regelen van allerlei dingen. Als je vragen hebt kan een email sturen naar :email',
    ],
    'phone-number' => 'Telefoonnummer',
];
