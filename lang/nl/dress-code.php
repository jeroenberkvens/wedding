<?php

return [
    'header'      => 'Kleding Voorschrift',
    'title'       => 'Black Tie Optional',
    'description' => 'Omdat we ons best hebben gedaan om een mooie bruiloft te organiseren hopen we dat jullie hierin kunnen helpen door het kleding voorschrift goed te lezen en te proberen je daar zo goed mogelijk aan te houden. Als je je afvraagt of iets past binnen het voorschrift of als je een er een andere vraag over hebt, stuur een email naar :email.',
    'men'      => [
        'title'       => 'Voor de Mannen',
        'description' => 'Bij "Black Tie Optional" is het uitgangspunt om te proberen in Black Tie te komen, maar het is geen probleem als dat niet lukt. Dus wat betekent dat precies? Mannen kunnen in plaats van de smoking die vereist is voor echte Black Tie ook een donker pak dragen (denk aan: je allerbeste pak, bij voorkeur zwart en op maat gemaakt).',
    ],
    'renting'  => [
        'title'       => 'Smoking Huren',
        'description' => 'Voor iedereen die geen pak heeft, is het natuurlijk ook altijd mogelijk om een Smoking te huren. Zelf haal ik mijn pak bij :blackTie in Den Bosch en zij verhuren ook smokings.',
    ],
    'women'    => [
        'title'       => 'Voor de Vrouwen',
        'description' => 'Waar vrouwen in Black Tie jurken tot op de grond horen te dragen kan je bij Black Tie Optional ook heel mooie cocktailjurken dragen in plaats van jurken tot op de grond, maar vermijd alles met onthullende uitsnijdingen of zoomlijnen boven de knie.',
    ],
    'shorter'  => [
        'title'       => 'Lengte & Kleur',
        'description' => 'Voor de lengte vragen we om een jurk te kiezen met een zoomlijnen onder de knie, en wat betreft kleur: :important gezien dat de kleuren zijn van de trouwjurk.',
        'important'   => 'geen rood of wit',
    ],
    'examples' => [
        'black-tie'      => 'Black-tie',
        'summer-wedding' => 'Zomerbruiloft',
        'too-short'      => 'Te Kort',
    ],
    'warning'  => [
        'title'       => 'Belangrijk!',
        'description' => 'Draag geen rode of witte jurk. Deze kleuren zijn gereserveerd voor de bruid.',
    ],
];
