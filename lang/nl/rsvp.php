<?php

return [
    'notice'            => 'Graag vragen we iedereen om huisdieren thuis te laten. Kinderen zijn welkom, maar houd ze alstublieft tijdens de ceremonie stil en excuseer uzelf indien nodig.',
    'name'              => 'Naam',
    'planning'          => 'Aanwezig',
    'allergens'         => 'Allergieën',
    'allergens-note'    => 'Voer hier alleen dingen in waar je echt alergisch voor bent',
    'diet-label'        => 'Dieetwensen',
    'diet-types'        => [
        'normal'     => 'Normaal',
        'vegetarian' => 'Vegetarisch',
        'vegan'      => 'Veganistisch',
    ],
    'diet'              => 'Overige dieetwensen',
    'diet-note'         => 'Voer hier alle overige wensen in',
    'none'              => '--- GEEN ---',
    'save'              => 'Opslaan',
    'updated'           => 'Wijzigingen Opgeslagen',
    'save-warning'      => [
        'title'       => 'Waarschuwing',
        'explanation' => 'Je RSVP is nog niet opgeslagen. Gebruik de "Opslaan" knop onder aan het formulier.',
    ],
    'email'             => [
        'header'    => 'RSVP aangepast',
        'body'      => ':actor heeft jou RSVP aangepast. Controleer of alles nog klopt.',
        'field'     => 'Veld',
        'value'     => 'Waarde',
        'button'    => 'Aanpassen',
        'greetings' => 'Met vriendelijke groet',
    ],
    'total'             => 'Alle Gasten',
    'party-only'        => 'Feest Gasten',
    'coming'            => 'Aanwezig',
    'not-coming'        => 'Afmeldingen',
    'user-without-rsvp' => 'Gebruikers zonder RSVP',
    'expected'          => 'Verwacht',
    'plus-one'          => 'Plus One',
];
