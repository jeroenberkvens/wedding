<?php

return [
    'header'     => 'Bruiloft Jeroen & Jenneke',
    'welcome'    => [
        'header' => 'Welkom',
        'title'  => 'Welkom op onze website',
        'body'   => [
            'Deze website gebruiken we om informatie te geven voor onze gasten.',
        ],
    ],
    'general'    => [
        'title' => 'Algemeen',
        'body'  => 'Verder hoef je ook geen fotos te maken, daar hebben we fotografen voor geregeld.',
    ],
    'location'   => [
        'title'      => 'Locatie',
        'body'       => 'De hele bruiloft en het feest vinden plaats op Landgoed Groot Warnsborn:',
        'directions' => 'Route',
    ],
    'rsvp'       => [
        'header' => 'RSVP',
        'text'   => 'Hier kun je voor jou en je partner / plus 1 / kinderen aangeven bij welke onderdelen je aanwezig bent en andere belangrijke informatie zoals dieetwensen.',
    ],
    'dress-code' => [
        'header' => 'Kleding Voorschrift',
        'text'   => 'Onze dress code is Black Tie Optional. Vind hier meer informatie over wat dat inhoud en belangrijke informatie over wat wel en niet gewenst is.',
    ],
];
