<?php

return [
    'intro'   => [
        'header'       => 'Belangrijke informatie',
        'descriptions' => [
            'Het lijkt ons leuk om middels de huwelijkscadeaus een nieuw servies te verzamelen. Hieronder staan alle onderdelen van ons gekozen servies met een link naar de webwinkel.',
            'Aangezien we een compleet en passend servies willen hebben is het belangrijk dat er alleen de onderdelen van dit specifieke servies gegeven worden.',
            'Tip: Klik eerst aan welk item en hoeveel je er koopt vóór de daadwerkelijke aankoop om verwarring te voorkomen voor andere bruiloftsgasten.',
            //            'Een envelopje wordt uiteraard ook zeer gewaardeerd.',
        ],
    ],
    'headers' => [
        'item'    => 'Product',
        'bought'  => 'Gekocht',
        'total'   => 'Totaal',
        'actions' => 'Acties',
    ],
    'fields' => [
        'amount' => 'Aantal',
    ],
    'actions' => [
        'cancel' => 'Nee, toch maar niet',
        'buy'    => 'Ik wil deze geven',
        'bought' => 'Ik heb er :count gekocht',
        'edit'   => 'Aanpassen',
    ],
    'notifications' => [
        'thanks' => 'Leuk dat je deze wilt kopen. Het staat genoteerd.',
        'never-mind' => 'We hebben de aanpassing opgeslagen.',
    ],
    'items'   => [
        'eetbord'                    => 'Dinerbord',
        'universeel-bord'            => 'Schaal',
        'ontbijtbord'                => 'Ontbijtbord',
        'broodbord'                  => 'Broodbord',
        'gourmetbord'                => 'Gourmetbord',
        'rechthoekige-serveerschaal' => 'Serveerschaal (groot)',
        'multifunctioneel-bord'      => 'Serveerschaal (klein)',
        'pastabord'                  => 'Pastabord',
        'dipschaaltje'               => 'Kommetje',
        'witte-wijnglas'             => 'Wijnglas (wit)',
        'rode-wijnglas'              => 'Wijnglas (rood)',
        'champagneglas'              => 'Champagneglas',
        'whiskyglas'                 => 'Drinkglas',
        'platte-schaal'              => 'Diep bord',
        'bol'                        => 'Soepkom',
        'koffieschotel'              => 'Koffieschotel',
        'espresso-schotel'           => 'Espressoschotel',
        'cafe-au-lait-schotel'       => 'Theeschotel',
        'koffiekop'                  => 'Koffiekop',
        'beker-l'                    => 'Beker (groot)',
        'espressokopje'              => 'Espressokopje',
        'beker-s'                    => 'Beker (klein)',
    ],
];
