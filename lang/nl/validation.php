<?php

return [
    'something' => 'Oeps! Er is iets mis gegaan.',
    'name'      => [
        'required' => 'Je moet je naam invullen.',
        'string'   => 'Je naam mag alleen letters en spaties hebben.',
        'regex'    => 'Je naam mag alleen letters en spaties hebben.',
        'max'      => 'Je naam is te lang.',
        'in'       => 'De gekozen naam komt niet voor in onze gastenlijst. Als je naam verkeerd of anders geschreven is kan je dit aanpassen in de RSVP.',
    ],
    'email'     => [
        'required'  => 'Het email adres is verplicht.',
        'something' => 'Het email adres lijkt niet te kloppen.',
        'max'       => 'Het email adres is te lang.',
        'exists'    => 'Het email adres is niet bij ons bekend.',
        'unique'    => 'Dit email adres is al door een ander in gebruik.',
    ],
    'amount'    => [
        'required' => 'Je moet het aantal doorgeven dat je gaat kopen.',
        'int'      => 'Het aantal dat je hebt ingevoerd is niet een cijfer.',
        'min'      => 'Het aantal kan niet minder dan 1 zijn. Als je deze toch niet wilt kopen klik dan die knop.',
        'max'      => 'Het lijkt er op dat je er meer wilt kopen dan er nog nodig zijn.',
    ],
    'allergens' => 'Er is iets mis gegaan met het invullen van je allergiën.',
    'dietType'  => [
        'required' => 'Kies een van de dieetwensen.',
        'string'   => 'Er is iets mis gegaan met het invullen van je dieetwensen.',
    ],
    'diet'      => 'Er is iets mis gegaan met het invullen van je overige dieetwensen.',
    'planning'  => 'Er is iets mis gegaan met het invullen van je planning opties.',
];
