<?php

return [
    'missing'            => 'Geen Reactie',
    'reacted'            => 'Gereageerd',
    'coming'             => 'Komen',
    'implied-not-coming' => 'Beheerder komt niet',
];
