<?php
declare(strict_types=1);

return [
    'email',
    'chantal berkvens',
    'black tie',
    'black tie optional',
    'home',
    'planning',
    'rsvp',
    'bistro diner',
    'black-tie',
    'login',
    'route',
    'account',
    'plus one',
];
